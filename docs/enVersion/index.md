---
title: ndmvr - NDimensional Virtual Reality
summary: Documentation for ndmvr project
authors:
    - Martin Fekete
some_url: https://ndmspc.gitlab.io/ndmvr
---
# JavaScript NDMVR

---

**NDMVR** is a package of the components that are used in the visualization of the histograms of the type such as TH in virtual reality.

The user guide contains a brief user manual for the interaction of the user in virtual reality because there is control displayed on the panel in virtual reality.

Mostly in this guidebook, we can see how to install this package and how to integrate the components into the client application. In addition to the integration itself, this guidebook also provides examples of how to build the applications on the top of the react with all the necessary libraries such as JSROOT and **NDMVR**.

The solution provides the react components, which we can use in any client application that is built with the [React](https://reactjs.org/docs/getting-started.html#try-react) for displaying the histograms. Using this package is to find the right intersection with more technologies for programmers. The programmer uses the components of this package in his implementation, and he has a possibility of building an application for his own purposes.

In addition to the visualization of the histograms, the library also has a component for displaying TH1 2D projections of the JSROOT histogram on the aside panel.

For the use of this technology, it is necessary to have an appropriate environment for development, which depends on [Node >= 10.16](https://nodejs.org/en/) and the package manager such as [npm](https://www.npmjs.com/package/@ndmspc/ndmvr). The components have been developed using library [React](https://reactjs.org/docs/getting-started.html#try-react). From that reason, it is possible to integrate only in applications with the react.

The next parts of the guidebook will describe the components of the library with examples for creating the client application. Then we can integrate components into the application. For full using of this library, it is necessary to ensure the acquisition of histograms from external servers and files.

Library for displaying TH histograms in virtual reality.

> #### Library provides:
>
> - ndmVrHistogramScene
> - ndmVrStorage
> - jsrootHistogram
>

# Visualization of ***NDMVR***

---

After loading the necessary histograms and importing all components from **NDMVR**, the user is able to interact with the histograms and the bins.

<figure>
  <img src="../assets/image1.png" width="1400"  alt="img1"/>
  <figcaption>Application view. On the left side of the screen, there is a scene with an imported histogram (ndmVrHistogramScene), on the right side of the screen there is displayed a projection.(jsrootHistogram).</figcaption>
</figure>

User can move around the scene, can mark the bins of the histogram and change the viewed histogram section. On the aside panel, there is a possibility of displaying a similar projection that is visualized as JSROOT SVG. Then the user can interact with it. The user movement is possible by wasd controls or moving in an immersive environment when using Oculus.

Bin's interaction is solved using raycaster, where the user focuses on the bin and does certain events such as mouseenter, mouseleave, click. The intersection is different and depends on the first intersected side of the bin.

---

Displaying controls for user's interactions in the VR scene. All necessary mappings are displaying on the panels directly in the VR. Mappings to switch modes with panels and histograms are displayed in the scene as well.

<figure>
  <img src="../assets/image2.png" width="1400"  alt="img2"/>
  <figcaption>View with displayed control panels. The user can rotate panels and change the displayed content.</figcaption>
</figure>

---

Displaying a projection for each bin. For more detailed analysis, the user can display the projection panels. The user can update the displayed projection view by pressing the key **enter**. The most important thing is displaying all projections as SVG elements on the aside panels. After that, the current view of the SVG element will be moved to the entity in the VR scene.

<figure>
  <img src="../assets/image3.png" width="1400"  alt="img3"/>
  <figcaption>View with displayed projection panels. User can rotate panels and change the displayed content. Each panel has space to display a different projection for the selected bin.</figcaption>
</figure>

Component **jsrootHistogram** actually contains 1 panel, but in the near future that component will be customized to define the user's own function for creating projections. A user can create a function that makes a special projection.

---

<figure>
  <img src="../assets/image4.png" width="1400"  alt="img4"/>
  <figcaption>To display the projections, after clicking on the bin, a preview of the projection is displayed on the board directly in the VR, so that the user can at least find out in detail the nature of the examined data.</figcaption>
</figure>