---
title: ndmvr - NDimensional Virtual Reality
summary: Documentation for ndmvr project
authors:
    - Martin Fekete
some_url: https://ndmspc.gitlab.io/ndmvr
---
# NdmVRStorage

---

Service manages offsets and ranges. Data are saved in localstorage for manipulation in next time.

- Service provide function for storing offsets:

```js
const section = {
  name: 'TH3',
  xOffset: 1,
  yOffset: 1,
  zOffset: 1,
  range: 8
}
storeOffsets(section)
```
- Service provide function for loading offsets:

```js
loadOffsets('TH3')
```

If the localstorage has stored data, we get returned value represented an object which determines the section.
