---
title: NDMVR - NDimensional Virtual Reality
summary: Documentation for ndmvr project
authors:
    - Martin Fekete
some_url: https://ndmspc.gitlab.io/ndmvr
---
# Vývoj webpacku

Riešenie bolo vyvíjané na zariadení s operačným systémom macOS Mojave v10.14.6.
Rovnako bol použitý Node.js verzie v12.18.4 a package manager npm verzie 6.14.11.
Pre samotný vývoj bolo použité vývojové prostredie WebStorm 2019.3.5.

Projekt predstavuje balík komponentov, ktoré sú exportované klientskej aplikácií.
Počas vývoja bola vytvorená demo aplikácia znázorňujúca použitie týchto exportovaných komponentov v klientskej aplikácií.

Celý projekt je zložený z dvoch jednoduchších projektov:

- projekt **ndmvr** slúžiaci na vývoj univerzálnych komponentov, ktoré budú neskôr inštalované v klientskych aplikáciách. Obsahuje svoje vlastné rozšírenia. Rovnako obsahuje aj súbor `package.json`, kde sú definované všetky potrebné rozšírenia a informácie o projekte.

- projekt **example** je klientská aplikácia, ktorá demonštruje použitie komponentov z **ndmvr**. Projekt je vložený v projekte ndmvr pre účely vývoja. Projekt obsahuje taktiež `package.json`.

---
## Projekt **ndmvr**

Štruktúra projektu
``` sh
ndmvr
├── ci/
├── dist/
├── docs/
├── etc/
├── example/
├── out/
├── src/
├── .editorconfig
├── .eslintignore
├── .eslintrc
├── .gitignore
├── .gitlab-ci.yml
├── .prettierrc
├── mkdocs.yml
├── package.json
├── package-lock.json
├── README.md
└── ...
```
Stručná charakteristika súborov:

- **dist** obsahuje výsledný preložený kód, build aplikácie ndmvr.
- **docs** obsahuje súbory nutné pre vytvorenie [mkdocs](https://www.mkdocs.org/) dokumentácie.
- **etc** obsahuje súborovú štruktúru, ktorá obsahuje všetky potrebné obrázky a pozadia pre komponenty.
- **example** obsahuje projekt example.
- **out** obsahuje všetky výsledné súbory vygenerované pomocou [jsdocs](https://jsdoc.app/).
- **src** obsahuje zdrojové kódy.
- **package.json** obsahujúci informácie o projekte a všetky potrebné závislosti, ktoré je nutné poskytnúť projektu.
- **README.md** obsahujúci informácie o projekte.
- Súbory definujúce konfigurácie nástrojov, použitých pri vývoji, ako napríklad eslint, prettier, gitlab.
- **mkdocs.yml** súbor slúžiaci pre vytvorenie výslednej mkdocs dokumentácie. Obsahuje informácie o dokumentácií, rozšírenia použité pri zostavení dokumentácie a iné konfiguračné atribúty.

Použité rozšírenia v projekte **ndmvr** na účel vývoja aplikácie
```js
"devDependencies": {
    "babel-eslint": "^10.0.3",
    "cross-env": "^7.0.2",
    "eslint": "^6.8.0",
    "eslint-config-prettier": "^6.7.0",
    "eslint-config-standard": "^14.1.0",
    "eslint-config-standard-react": "^9.2.0",
    "eslint-plugin-import": "^2.18.2",
    "eslint-plugin-node": "^11.0.0",
    "eslint-plugin-prettier": "^3.1.1",
    "eslint-plugin-promise": "^4.2.1",
    "eslint-plugin-react": "^7.17.0",
    "eslint-plugin-standard": "^4.0.1",
    "jsdoc": "^3.6.7",
    "microbundle-crl": "^0.13.10",
    "npm-run-all": "^4.1.5",
    "prettier": "^2.0.4",
    "react": "^16.13.1",
    "react-dom": "^16.13.1",
    "react-scripts": "^3.4.1"
  }
```

Použité rozšírenia v projekte **ndmvr** na účel vytvorenia produkčnej aplikácie
```js
"dependencies": {
    "@obl/react-jsroot": "0.0.2",
    "rxjs": "^6.6.3"
  }
```
---

## Projekt **example**

Štruktúra projektu
``` sh
ndmvr/example
├── public/
├── src/
├── package.json
├── package-lock.json
├── README.md
└── ...
```
Stručná charakteristika súborov:

- **public** pre verejne dostupné súbory na webovom serveri.
- **src** obsahuje zdrojové kódy.
- **package.json** obsahujúci informácie o projekte a všetky potrebné závislosti, ktoré je nutné poskytnúť projektu.
- **README.md** obsahujúci informácie o projekte.

Použité rozšírenia v projekte **example** na účel vývoja aplikácie
```js
"devDependencies": {
    "@babel/plugin-syntax-object-rest-spread": "^7.8.3"
  }
```

Použité rozšírenia v projekte **example** na účel vytvorenia produkčnej aplikácie
```js
"dependencies": {
    "@material-ui/core": "^4.11.0",
    "@material-ui/icons": "^4.9.1",
    "@ndmspc/react-jsroot": "^0.0.2",
    "aframe": "^1.0.4",
    "@ndmspc/ndmvr": "file:..",
    "react": "file:../node_modules/react",
    "react-dom": "file:../node_modules/react-dom",
    "react-router-dom": "^5.2.0",
    "react-scripts": "file:../node_modules/react-scripts"
  }
```
---
