---
title: NDMVR - NDimensional Virtual Reality
summary: Documentation for ndmvr project
authors:
    - Martin Fekete
some_url: https://ndmspc.gitlab.io/ndmvr
---
# Adresár ***utils***

Adresár obsahuje triedy, ktorých inštancie slúžia pre generovanie entít, z ktorých bude postavená scéna a histogram.

Logika, ktorú riešenie obsahuje je obsiahnutá v triedach v tomto adresári.

---

HistogramReactFactory - Histogram generator

## new HistogramReactFactory(uniqueName, histogram, section, range, projections, theme)

---

Inicializácia objektu pre vytváranie elementov histogramu. Objekt obsahuje funkcie, ktoré umožnia vytvorenie entít pre oba typy histogramov, a to TH2 a TH3. Objekt obsahuje funkcie, ktoré slúžia na získanie informácií o histograme, ako sú ofsety, rozsah.

| Param | Type | Description |
| --- | --- | --- |
| uniqueName | <code>string</code> | Identifikátor histogramu |
| histogram | <code>Object</code> | Objekt histogramu určený na vizualizáciu |
| section | <code>Object</code> | Objekt definujúci ofsety a rozsah histogramu |
| range | <code>number</code> | Rozsah, ak sa nedefinujú ofsety ale len rozsah |
| projections | <code>Object</code> | Objekt definujúci súborovú štruktúru s projekciami |
| theme | <code>string</code> | string Definujúci názov témy, ktorá má byť použitá pri vizualizovaní |

## getXOffset ⇒ <code>number</code>

---
Vracia atribút objektu offset x.

**Návratová hodnota**: <code>number</code> - offsetX - Offset na osi x

## getYOffset ⇒ <code>number</code>

---
Vracia atribút objektu offset y.

**Návratová hodnota**: <code>number</code> - offsetX - Offset na osi y

## getZOffset ⇒ <code>number</code>

---
Vracia atribút objektu offset y.

**Návratová hodnota**: <code>number</code> - offsetX - Offset na osi y

## getRange ⇒ <code>number</code>

---
Vracia atribút objektu range.

**Návratová hodnota**: <code>number</code> - range - Vracia rozsah sekcie

## initializeFactory(uniqueName, histogram, section, range, projections, theme) ⇒ <code>void</code>

---
Inicializácia objektu pre vytváranie elementov histogramu.

| Param | Type | Description |
| --- | --- | --- |
| uniqueName | <code>string</code> | Identifikátor histogramu |
| histogram | <code>Object</code> | Objekt histogramu určený na vizualizáciu |
| section | <code>Object</code> | Objekt definujúci ofsety a rozsah histogramu |
| range | <code>number</code> | Rozsah, ak sa nedefinujú ofsety ale len rozsah |
| projections | <code>Object</code> | Objekt definujúci súborovú štruktúru s projekciami |
| theme | <code>string</code> | String definujúci názov témy, ktorá má byť použitá pri vizualizovaní |

## createTH3Histogram() ⇒ <code>Object</code>

---
Funkcia na základe atribútov objektu vytvorí všetky potrebné entity pre histogram TH3.

**Návratová hodnota**: <code>Object</code> - elements - Vracia objekt obsahujúci roztriedené všetky potrebné entity histogramu pre VR

## createTH2Histogram(projectionFunction) ⇒ <code>Object</code>

---
Funkcia na základe atribútov objektu vytvorí všetky potrebné entity pre histogram TH2.

**Návratová hodnota**: <code>Object</code> - elements - vracia objekt obsahujúci roztriedené všetky potrebné entity histogramu pre VR

| Param | Type | Description |
| --- | --- | --- |
| projectionFunction | <code>string</code> | parameter určuje, ktorý vizualizačný mód bude použitý pri vytvorení entít |

