---
title: NDMVR - NDimensional Virtual Reality
summary: Documentation for ndmvr project
authors:
    - Martin Fekete
some_url: https://ndmspc.gitlab.io/ndmvr
---
# JavaScript ***NDMVR***

---

**NDMVR** je balík komponentov, ktoré sú uplatniteľné za účelom vizualizácie histogramov typu TH vo virtuálnej realite.

Používateľská príručka preto obsahuje stručný používateľský manuál pre interakciu používateľa vo virtuálnej realite vzhľadom na to, že podrobné ovládanie je znázornené priamo na paneloch vo virtuálnej realite.

Najviac bude v príručke popísané ako nainštalovať tento balík komponentov a ako ho integrovať do klientskej aplikácie tak, aby všetko fungovalo správne. Okrem integrácie budú v príručke znázornené aj príklady, ktoré nesúvisia priamo s balíkom komponentov **ndmvr** ale sú užitočné pre vytvorenie kvalitnej klientskej aplikácie na dátovú analýzu.

Riešenie predstavuje komponenty, ktoré sa dajú použiť v ľubovoľnej klientskej aplikácii založenej na [React](https://reactjs.org/docs/getting-started.html#try-react) na vizualizáciu rôznych histogramov.
Použitie tejto knižnice spočíva v integrácii týchto komponentov programátorom do klientskej aplikácie pre vizualizáciu dát, špeciálne pre danú aplikáciu.
Okrem samotnej vizualizácie histogramov vo virtuálnej realite. Knižnica obsahuje aj pomocný komponent pre zobrazenie projekcie na bočnom panely.

Pre použitie tejto technológie je potrebné mať vhodné prostredie pre vývoj webovej aplikácie založenej na [Node >= 10.16](https://nodejs.org/en/) a aj package manager, ako napríklad [npm](https://www.npmjs.com/package/@ndmspc/ndmvr).
Keďže komponenty boli vyvíjané použitím knižnice [React](https://reactjs.org/docs/getting-started.html#try-react), tak preto je možné integrovať len do aplikácie založenej na React-e.

Nasledujúce časti preto budú popisovať komponenty knižnice aj s príkladmi pre vytvorenie klientskej aplikácie, do ktorej je možné tieto komponenty integrovať.
Pre plnohodnotné využitie tejto knižnice je potrebné mať zabezpečené získavanie histogramov, ktoré sú určené na vizualizáciu.
Na tento účel je potrebné máť zahrnutú aj knižnicu [JSROOT](https://github.com/root-project/jsroot/blob/master/docs/JSROOT.md), ktorá umožní vytváranie alebo získavanie histogramov z ROOT súborov alebo vzdialených serverov.

Knižnica obsahuje komponenty určené na vizualizáciu histogramov vo virtuálnej realite.

> #### Knižnica poskytuje komponenty:
>
> - ndmVrHistogramScene
> - ndmVrStorage
> - jsrootHistogram
>

### komponent ***ndmVrHistogramScene***

Vytvorenie VR scény spolu s histogramom a kamerou. Obsahuje všetky potrebné časti, dôležité pre správnu interaktivitu používateľa s histogramom priamo v scéne.

### komponent ***ndmVrStorage***

Vytvorí servis pre menežment lokálneho úložiska. Obsahuje metódy, ktorými je možné uložiť a následne načítať údaje o zobrazovaných sekciách histogramu pre prípad neskoršieho vrátenia sa k vizualizácií. Je to optimalizačný komponent a nie je potrebný pre správne fungovanie a interaktivitu.

### komponent ***jsrootHistogram***

Vytvorí komponent pre vizualizáciu 2D histogramov pomocou JSROOT. Vizualizácia histogramu bez použitia VR. Obsahuje prepojenie s komponentom **ndmVrHistogramScene** kde aktuálny náhľad v elemente, ktorý zobrazuje interaktívny 2D histogram je možné zobraziť aj na entite vo virtuálnej realite.

# Vizualizácia ***NDMVR***

---

Po načítaní všetkých potrebných histogramov a vložení komponentov NDMVR do scény, používateľ dokáže interagovať s histogramom a binmi v scéne rovnako, ako aj voľne sa pohybovať v scéne klávesami **wasd**.

<figure>
  <img src="../assets/image1.png" width="1400"  alt="img1"/>
  <figcaption>Náhľad aplikácie. Vľavo je náhľad scény s histogramom (ndmVrHistogramScene), vpravo je zobrazená projekcia (jsrootHistogram).</figcaption>
</figure>

Používateľ sa môže voľne pohybovať v scéne, označovať biny a meniť zobrazované sekcie histogramu. Na bočnom paneli si môže následne používateľ zobraziť podrobnú projekciu rovnako, ako aj manipulovať so zobrazeným histogramom.
Pohyb používateľa je možný klávesami **wasd**, alebo voľným pohybom v priestore ak je použité zariadenie Oculus. Interakcia s binmi je riešená sústredením sa na bin a eventmi, ako sú klik, hover.

---

Zobrazenie ovládania pre manipuláciu s histogramom vo VR scéne. Klávesy pre zobrazenie panelov sú k dispozícii pre používateľa priamo v scéne na bočných paneloch obsahujúcich názvy zobrazovaných dát a súboru.

<figure>
  <img src="../assets/image2.png" width="1400"  alt="img2"/>
  <figcaption>Náhľad zobrazenia panelov s ovládaním. Používateľ môže panely rotovať a meniť zobrazovaný obsah.</figcaption>
</figure>

---

Zobrazenie projekcií pre každý bin. Pre podrobnejšie analyzovanie binov si môže používateľ zobraziť panely, na ktorých budú zobrazené histogramy pre daný bin. Používateľ si taktiež môže aj aktualizovať aktuálny pohľad klávesou **enter**. Dôležité je aby boli na postrannom paneli zobrazené potrebné projekcie, ktorých náhľad sa prenesie na panely vo VR.

<figure>
  <img src="../assets/image3.png" width="1400"  alt="img3"/>
  <figcaption>Náhľad zobrazenia panelov s projekciami. Používateľ môže panely rotovať a prepnúť zobrazovaný obsah. Na každom paneli je priestor pre zobrazenie inej projekcie pre zvolený bin.</figcaption>
</figure>

Komponent **jsrootHistogram** obsahuje 1 panel, ale v blízkej budúcnosti tento komponent bude prispôsobený, aby bolo možné definovať vlastné funkcie, kde si používateľ sám zadefinuje funkcie vytvárajúce projekcie pre biny podľa svojej potreby.

---

<figure>
  <img src="../assets/image4.png" width="1400"  alt="img4"/>
  <figcaption>Pre zobrazovanie projekcií je po klinutí na bin zobrazovaný náhľad projekcie na tabuli priamo vo VR, aby používateľ vedel aspoň podrobne zistiť charakter skúmaných dát.</figcaption>
</figure>
