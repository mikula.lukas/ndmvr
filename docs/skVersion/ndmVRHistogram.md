---
title: NDMVR - NdmVrHistogramScene
summary: Dokumentácia pre NDMVR
authors:
    - Martin Fekete
some_url: https://ndmspc.gitlab.io/ndmvr
---

# ***NdmVRHistogramScene***

---

Komponent je určený hlavne na vytvorenie VR scény spolu s potrebnými komponentami pre zobrazenie histogramu TH a kamrery aby bola zabezpečená interaktivita medzi používateľom a histogramom. Komponent vyžaduje 2 parametre, a to parameter **data** a **info**.

```jsx
<NdmVrScene data={data} info={info} />
```

Oba parametre sú objekty obsahujúce náležité údaje dôležité pre vytvorenie vizualizácie.

- **data** - hlavné údaje, ako objekt histogramu, údaje o sekcii, ktorá sa má zobraziť a adresárová štruktúra s projekciami.
- **info** - sekundárne údaje, ako samotné pozadie scény s atribútmi, téma pre vizualizáciu histogramu.

## Atribút ***data***

| Atribút | Typ | Popis | Predvolená hodnota | Voliteľný |
| ------ | ------ | ------ | ------ | ------ |
| **histogram** | Object | Hlavný objekt histogramu určený na vizualizáciu | `null` | `False` |
| **projections** | Object | Súborová štruktúra obsahujúca projekcie | `null` | `True` |
| **section** |  Object | Objekt obsahuje ofsety a rozsah pre zobrazenie určitej časti histogramu | -- | `True` |

### objekt ***histogram***

Tento objekt definuje hlavný histogram TH.

Najčastejšie dostaneme tento objekt načítaním z **ROOT súboru** alebo vytvorením pomocou funkcií knižnice [JSROOT](https://github.com/root-project/jsroot/blob/master/docs/JSROOT.md).

| Atribút | Typ | Popis | Predvolená hodnota | Voliteľný |
| ------ | ------ | ------ | ------ | ------ |
| **histogram** | Object | Hlavný objekt histogramu určený na vizualizáciu | `null` | `False` |

### objekt ***projections***

Súborová štruktúra obsahuje organizované adresáre a projekcie pre každý bin hlavného histogramu.

Samotná projekcia je taktiež objekt histogramu. Tento histogram obsahuje konkrétnejšie dáta. Každý bin hlavného histogramu môže obsahovať viac projekcií zamerané na konkrétnu informáciu.

| Atribút | Typ | Popis | Predvolená hodnota | Voliteľný |
| ------ | ------ | ------ | ------ | ------ |
| **projections** | Object | Súborová štruktúra obsahujúca projekcie | `null` | `True` |

Štruktúra súborov `root` môže vyzerať nasledovne:

``` sh
.root
├── 1
│   └── 1
│   │   └── projectionObject
│   │   └── ...
│   └── 3
│   └── 4
│   └── ...
├── 2
│   └── 1
│   └── 2
│   └── ...
└── ...
```

Zobrazená súborová štruktúra opisuje vzor, kde každý bin má svoje projekcie adresované v adresároch s cestou, ktorá určuje súradnice binu.
Ako môžme vidieť, bin s x súradnicou = 1 a y súradnica = 1 má projekcie uložené v adresári s cestou `root/1/1/projArray[0]`

### object ***section***

Objekt definuje dáta pre vizualizáciu určitého množstva dát. Dáta su zobrazené v sekcii, ktorá je určená ofsetmi na jednotlivých osiach a rozsahom, ktorý definuje koľko binov na danej osi bude zobrazených. Rozsah je rovnaký pre všetky osi histogramu.

- V prípade TH2 histogramu existujú ofsety pre 2 osi (x, y)
- V prípade TH3 histogramu existujú ofsety pre 3 osi (x, y, z)

Rozsah definuje koľko binov bude zobrazených na jednotlivých osiach. Ak bude rozsah sekcie histogramu TH3 nastavený na 4, následne bude počet všetkých zobrazených binov v pomere `x(4+1)` * `y(4+1)` * `z(4+1)` čo činí 125 binov.

Objekt by mal vyzerať nasledovne:

``` json
{
  name: 'TH3',
  xOffset: 1,
  yOffset: 1,
  zOffset: 1,
  range: 4
}
```

``` json
{
  name: 'TH2',
  xOffset: 1,
  yOffset: 1,
  range: 8
}
```

## Atribút ***info***

Atribút je používaný na definovanie informácií o histograme a farebné témy.

Objekt definuje tvar a **url** obrázka pozadia, používateľ by mal použiť svoj obrázok, ktorý by mal byť uložený v adresári `public` klientskej aplikácie.

- Hlavné atribúty objektu **info**:

| Atribút | Typ | Popis | Predvolená hodnota | Voliteľný |
| ------ | ------ | ------ | ------ | ------ |
| **name** | String | Unikátne meno histogramu | `null` | `True` |
| **theme** | String | Reťazec definujúci názov témy | `def` | `False` |
| **background** | Object | objekt obsahujúci atribúty pozadia VR scény | `null` | `True` |

- Hlavné atribúty objektu **background**:

| Atribút | Typ | Popis | Predvolená hodnota | Voliteľný |
| ------ | ------ | ------ | ------ | ------ |
| **url** | String | definuje cestu k obrázku uloženého v public adresári | `./ndmvr/backgrounds/background1.jpg` | `False` |
| **radius** | number | rozmer VR priestoru definovaný polomerom | `3000` | `False` |
| **height** | number | výška obrázka | `2048` | `False` |
| **width** | number | šírka obrázka | `2048` | `False` |

Objekt by mal vyzerať nasledovne:

``` json
{
  name: 'histogram unique name',
  theme: 'string characterizing the theme',
  background: {
      url: 'image url',
      radius: 'radius of the background in VR',
      height: 'height',
      width: 'height'
  }
}
```
