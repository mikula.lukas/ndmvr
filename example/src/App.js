import React, { useContext } from 'react'
import { makeStyles } from '@mui/styles'
import { Typography, CssBaseline, LinearProgress } from '@mui/material'
import { Redirect, Route, Switch } from 'react-router-dom'
import Navbar from './molecules/Navbar'
import AframeReactTH2 from './pages/AframeReactTH2'
import AframeReactTH3 from './pages/AframeReactTH3'
// import Settings from './pages/Settings'
// import { ndmVrStorage } from '@ndmspc/ndmvr'
import { JSROOTContext } from '@ndmspc/react-jsroot'
import THProjection from './pages/THProjection'

const useStyles = makeStyles(() => ({
  headline: {
    marginTop: '0.5rem',
    fontWeight: 'bold',
    opacity: 0.6
  }
}))

const App = () => {
  // clear local storage on render app
  // const storage = window.localStorage
  // storage.clear()
  const classes = useStyles()
  // ndmVrStorage.initLocalStorage()
  // ndmVrStorage.storeFilePath('https://eos.ndmspc.io/eos/ndmspc/scratch/test/gitlab-hist.root')

  // // define the histogram section to display and save to localstorage
  // let histogramSectionTH3 = {
  //   name: 'TH3',
  //   xOffset: 1,
  //   yOffset: 1,
  //   zOffset: 1,
  //   range: 11
  // }
  // let histogramSectionTH2 = {
  //   name: 'TH2',
  //   xOffset: 1,
  //   yOffset: 1,
  //   range: 15
  // }
  // storage.setItem('TH3Offsets', JSON.stringify(histogramSectionTH3))
  // storage.setItem('TH2Offsets', JSON.stringify(histogramSectionTH2))
  // storage.setItem(
  //   'rootFile',
  //   'https://eos-hlit.jinr.ru/eos/hybrilit.jinr.ru/scratch/ndmspc-gitlab.root'
  // )
  // localStorageService.storeTH2Offsets(1, 1, 12)
  // localStorageService.storeTH3Offsets(1, 1, 1, 6)
  // localStorageService.storeFilePath(
  //    'https://eos-hlit.jinr.ru/eos/hybrilit.jinr.ru/scratch/ndmspc-gitlab.root'
  // )
  const status = useContext(JSROOTContext)
  return (
    <React.Fragment>
      {(status === 'ready' && (
        <div>
          <CssBaseline />
          <Navbar />
          <Switch>
            <Route exact path='/aframe/th3demo'>
              <AframeReactTH2 />
            </Route>
            <Route exact path='/aframe/th3'>
              <AframeReactTH3 />
            </Route>
            <Route exact path='/aframe/thProjection'>
              <THProjection />
            </Route>
            {/*<Route exact path='/settings'>
              <Settings />
            </Route>*/}
            <Route path='/'>
              <Redirect to='/aframe/th3demo' />
            </Route>
          </Switch>
        </div>
      ))
      ||
      <div>
        <Typography align='center' variant='h3' className={classes.headline}>JSROOT loading</Typography>
        <LinearProgress />
      </div>}
    </React.Fragment>
  )
}

export default App
