import {
  Icon,
  ListItem,
  ListItemIcon,
  ListItemText,
} from "@mui/material";
import { makeStyles } from "@mui/styles";
import React from "react";
import { Link } from "react-router-dom";

const useStyle = makeStyles({
  link: {
    textDecoration: "none",
    color: "black",
  },
});

const Item = ({ name, icon, url }) => {
  const classes = useStyle();
  return (
    <Link to={url} className={classes.link}>
      <ListItem button>
        <ListItemIcon>
          <Icon>{icon}</Icon>
        </ListItemIcon>
        <ListItemText primary={name} />
      </ListItem>
    </Link>
  );
};

export default Item;
