import './index.css'

import React from 'react'
import ReactDOM from 'react-dom'
import App from './App'
import { HashRouter as Router } from 'react-router-dom'
import * as serviceWorker from './serviceWorker'
import { JSROOTProvider } from '@ndmspc/react-jsroot'

ReactDOM.render(
  <Router>
    <JSROOTProvider>
      <App />
    </JSROOTProvider>
  </Router>,
  document.getElementById('root')
)

serviceWorker.unregister()
