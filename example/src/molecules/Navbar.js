import {
  AppBar,
  // Icon,
  IconButton,
  Toolbar,
  Typography
} from '@mui/material'
import { makeStyles } from "@mui/styles";
import MenuIcon from '@mui/material/Menu'
import React, { useState } from 'react'
// import { Link, useHistory } from 'react-router-dom'
import Sidebar from "./Sidebar";

const useStyle = makeStyles({
  wrapper: {
    flexGrow: 1
  },
  flex: {
    flexGrow: 1,
    color: '#fff'
  },
  link: {
    textDecoration: 'none',
    color: 'inherit'
  }
})

const Navbar = () => {
  const [openDrawer, setOpenDrawer] = useState(false)
  // const history = useHistory()
  const classes = useStyle()

  return (
    <div className={classes.wrapper}>
      <Sidebar open={openDrawer} toggle={(close) => setOpenDrawer(close)} />
      <AppBar position='static'>
        <Toolbar>
          <IconButton
            color='inherit'
            aria-label='Menu'
            onClick={() => setOpenDrawer(true)}
          >
            <MenuIcon />
          </IconButton>
          <Typography
            variant='subtitle1'
            color='inherit'
            className={classes.flex}
          >
            NDM VR
          </Typography>
        </Toolbar>
      </AppBar>
    </div>
  )
}

export default Navbar
