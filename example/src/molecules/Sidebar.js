import { Drawer, List } from '@mui/material'
import { makeStyles } from "@mui/styles";
import React from 'react'
import Item from '../atoms/Item'

const getSidebarItems = () => {
  const items = [
    {
      name: 'TH3Demo',
      icon: 'web',
      url: '/aframe/th3demo'
    },
    {
      name: 'TH3Example',
      icon: 'web',
      url: '/aframe/th3'
    },
    {
      name: 'TH2Example',
      icon: 'web',
      url: '/aframe/thProjection'
    }/*,
    {
      name: 'Settings',
      icon: 'settings',
      url: '/settings'
    }*/
  ]

  return items
}

const useStyle = makeStyles({
  list: {
    width: 250
  }
})

const renderItems = (services) => {
  return getSidebarItems(services).map((item, index) => (
    <Item key={index} name={item.name} icon={item.icon} url={item.url} />
  ))
}

const Sidebar = ({ open, toggle }) => {
  const classes = useStyle()

  return (
    <Drawer open={open} onClose={() => toggle(false)}>
      <div
        tabIndex={0}
        role='button'
        onClick={() => toggle(false)}
        onKeyDown={() => toggle(false)}
      >
        <div className={classes.list}>
          <List>{renderItems()}</List>
        </div>
      </div>
    </Drawer>
  )
}

export default Sidebar
