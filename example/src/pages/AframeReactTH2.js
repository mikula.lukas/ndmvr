import React, { useEffect, useState } from 'react'
import { NdmVrScene } from '@ndmspc/ndmvr'
import { createDemoTH3Example } from '../utilsScripts/utilsPageScript'
import { LinearProgress } from '@mui/material'

const AframeReactTH2 = () => {
  const [histogramLoaded, setHistogramLoaded] = useState(false)
  const [histogram, setHistogram] = useState(null)
  const JSROOT = window.JSROOT

  const background = {
    url: './ndmvr/backgrounds/background1.jpg',
    radius: '3000',
    height: '2048'
  }

  useEffect(() => {
    const histo = createDemoTH3Example(JSROOT)
    setHistogram(histo)
    setHistogramLoaded(true)
  }, [JSROOT])

  useEffect(() => {
    const timer = setInterval(() => {
      const histo = createDemoTH3Example(JSROOT)
      setHistogram(histo)
    }, 8000)
    return () => clearTimeout(timer)
  }, [JSROOT])

  return (
    <React.Fragment>
      {histogramLoaded !== false ?
        <NdmVrScene data={{histogram: histogram, range: 4,  projections: null}} info={{name: 'th3gitlab', theme: 'nebula', background: background}} />
        :
        <LinearProgress />
      }
    </React.Fragment>
  )
}

export default AframeReactTH2
