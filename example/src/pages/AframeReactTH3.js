import React, { useEffect, useState } from 'react'
import { NdmVrScene } from '@ndmspc/ndmvr'
import { LinearProgress } from '@mui/material'

const AframeReactTH3 = () => {
  const [histogramLoaded, setHistogramLoaded] = useState(false)
  /* const [section] = useState((ndmVrStorage.loadOffsets('TH3') !== null)?ndmVrStorage.loadOffsets('TH3'):{
    name: 'TH3',
    range: 4
  }) */
  const [histogram, setHistogram] = useState(null)
  const JSROOT = window.JSROOT

  // get item from local storage
  // const storage = window.localStorage
  // const section = {
  //  name: 'TH3',
  //  xOffset: 1,
  //  yOffset: 1,
  //  zOffset: 1,
  //  range: 6
  // }
  // const section = (ndmVrStorage.loadOffsets('TH3') !== null)?ndmVrStorage.loadOffsets('TH3'):{
  //  name: 'TH3',
  //  xOffset: 1,
  //  yOffset: 1,
  //  zOffset: 1,
  //  range: 4
  // }

  //open file and load histogram
  /*
  useEffect(() => {

    // if (JSROOT === undefined) return
    if (!histogramLoaded) {
      // const filename = 'https://eos-hlit.jinr.ru/eos/hybrilit.jinr.ru/scratch/ndmspc-gitlab.root'
      //const filename = '/ndmspc-gitlab.root'
      const filename = 'https://eos-hlit.jinr.ru/eos/hybrilit.jinr.ru/scratch/ndmspc-gitlab.root'
      JSROOT.openFile(filename)
        .then(file => file.readObject('hUsersVsProjectsVsState'))
        .then(obj => {
          setHistogram(obj)
          setHistogramLoaded(true)
        })
        .then(() => console.log("drawing completed"))
        .catch(error => {
          console.log(error)
          const histo = createDemoTH3Histogram(JSROOT)
          console.log(histo)
          setHistogram(histo)
          setHistogramLoaded(true)
        });
    }
  }, [histogramLoaded, JSROOT])
   */

  const background = {
    url: '',
    radius: '3000',
    height: '2048'
  }
 //open file and load histogram

 useEffect(() => {
  if(!histogramLoaded) {
    //const filename = 'https://eos-hlit.jinr.ru/eos/hybrilit.jinr.ru/scratch/ndmspc-gitlab.root'
    //const filename = '/ndmspc-git
    let filename = 'https://eos.ndmspc.io/eos/ndmspc/scratch/daos/test/mon_kv_8.root'
    JSROOT.openFile(filename)
      .then(file => {
        console.log(file)
        return file.readObject('hSpeedAvgNFiles')
      })
      .then(obj => {
        console.log(obj)
        setHistogram(obj)
        setHistogramLoaded(true)
      })
      .then(() => console.log("drawing completed"))
      .catch(error => {
        console.log(error)
      });
  }
}, [JSROOT, histogramLoaded])

  return (
    <React.Fragment>
      {histogramLoaded !== false ?
        <NdmVrScene data={{histogram: histogram, range: 30,  projections: null}} info={{name: 'th3gitlab', theme: 'water', background: background}} />
        :
        <LinearProgress />
      }
    </React.Fragment>
  )
}

export default AframeReactTH3
