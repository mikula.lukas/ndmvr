import React, { useState, useEffect } from 'react'
import {
  Grid,
  Slide,
  Button,
  //GridList,
  //LinearProgress,
  //IconButton,
  //Card,
  //CardContent,
  //CardActions,
  CircularProgress,
  FormControl,
  InputLabel,
  Select,
  Container,
  DialogActions,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
} from '@mui/material'
import { makeStyles } from "@mui/styles";
// import StarBorderIcon from '@material-ui/icons/StarBorder';
import { NdmVrScene, JSrootHistogram, binDataDistributor } from '@ndmspc/ndmvr'
// import Typography from '@material-ui/core/Typography'
// import { createDemoTH2Histogram } from '../utilsScripts/utilsPageScript'

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  headline: {
    marginTop: theme.spacing(4),
    marginBottom: theme.spacing(3)
  },
  dataSection: {
    background: 'lightgrey'
  },
  gridRow: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    overflow: 'hidden',
    backgroundColor: theme.palette.primary.light,
  },
  gridList: {
    padding: '0.5rem',
    flexWrap: 'nowrap',
    transform: 'translateZ(0)',
    background: theme.palette.primary.light,
  },
  title: {
    color: theme.palette.primary.light,
  },
  hiddenElm: {
    display: 'none'
  },
  card: {
    padding: theme.spacing(2),
    marginLeft: theme.spacing(0.5),
    marginRight: theme.spacing(0.5),
    minWidth: 175,
    maxWidth: 400
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  cardTitle: {
    fontSize: 18,
  },
  pos: {
    marginBottom: 12,
  },
  circularProgress: {
    display: 'block',
    margin: 'auto'
  },
  loadProgress: {
    marginTop: theme.spacing(4),
    display: 'block',
    margin: 'auto'
  },
  formControl: {
    display: 'flex',
    justifyContent: 'center',
    margin: theme.spacing(1),
    minWidth: 120,
    paddingBottom: 20,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
  showOptions: {
    width: '100%',
    margin: 0,
    backgroundColor: '#403f3f',
    fontWeight: 'bold',
    color: theme.palette.primary.light
  },
  showDialog: {
    width: '100%',
    margin: 0,
    backgroundColor: theme.palette.primary.light,
    fontWeight: 'bold',
    color: '#403f3f'
  },
  optionsContainer: {
    width: '100%',
    backgroundColor: '#b1b1b1',
    margin: 0,
    padding: 0,
    paddingTop: theme.spacing(1)
  },
}))

const THProjection = () => {

  const classes = useStyles()
  // state for displaying all selected bins
  // const [show] = useState(false)
  // const [selectedBins, setSelectedBins] = useState([])
  const [file, setFile] = useState(null)
  // is histogram loaded
  const [histogramLoaded, setHistogramLoaded] = useState(false)
  const [histograms, setHistograms] = useState(null)
  const [theme, setTheme] = useState('def')
  const [option, setOption] = useState('gitlab')
  const [range, setRange] = useState(4)
  const [expanded, setExpanded] = useState(false)
  const [selectedBins, setSelectedBins] = useState([])
  const [focusedBin, setFocusedBin] = useState({})
  const [openDia, setOpenDia] = useState(false);
  // get histogram section from localstorage
  // const section = {
  //  name: 'TH2',
  //  xOffset: 1,
  //  yOffset: 1,
  //  range: 20
  //}
  // const section = {
  //    name: 'TH2',
  //    xOffset: 1,
  //    yOffset: 1,
  //    range: 4
  // }

  const handleChange = (e) => {
    console.log(e.target.value)
    setOption((prev) => (prev !== e.target.value)? e.target.value : prev)
  }

  const handleThemeChange = (e) => {
    setTheme((prev) => (prev !== e.target.value)? e.target.value : prev)
  }

  const handleRangeChange = (e) => {
    setRange(Number.parseInt(e.target.value))
  }

  const handleBinFocusChange = (e) => {
    setFocusedBin(JSON.parse(e.target.value))
  }

  const handleClickOpen = () => {
    setOpenDia(true);
  };

  const handleClose = () => {
    setOpenDia(false);
  };

  const getData = () => {
    let data
    if (option === 'gitlab') {
      data = {
        histogram: histograms.gitlab,
        projections: null,
        section: null,
        range: range
      }
    } else if (option === 'physics') {
      data = {
        histogram: histograms.physics,
        projections: histograms.projections,
        section: null,
        range: range
      }
    } else if (option === 'daosPhysics') {
      data = {
        histogram: histograms.daosPhysics,
        projections: null,
        section: null,
        range: range
      }
    } else if (option === 'daosHTime') {
        data = {
          histogram: histograms.daosHTime,
          projections: null,
          section: null,
          range: range
        }
    } else if (option === 'daosHW') {
      data = {
        histogram: histograms.daosHW,
        projections: null,
        section: null,
        range: range
      }
    } else if (option === 'daosHSpeedAvg') {
      data = {
        histogram: histograms.daosHSpeedAvg,
        projections: null,
        section: null,
        range: range
      }
    } else {
      data = {
        histogram: histograms.cern,
        projections: null,
        section: null,
        range: range
      }
    }

    return data
  }

  const getInfo = () => {
    let info
    if (option === 'gitlab') {
      info = {
        name: 'Gitlab',
        theme: theme,
        background: {
          url: './ndmvr/backgrounds/background1.jpg',
          radius: '3000',
          height: '2048',
          yPosition: '1000',
          width: '2048'
        }
      }
    } else if (option === 'physics') {
      info = {
        name: 'Physics',
        theme: theme,
        background: {
          url: './ndmvr/backgrounds/govorun.jpg',
          radius: '3000',
          height: '1000',
          width: '2048'
        }
      }
    } else if (option === 'daosHTime') {
      info = {
        name: 'hTime',
        theme: theme,
        background: {
          url: './ndmvr/backgrounds/govorun.jpg',
          radius: '3000',
          yPosition: '0',
          height: '2048',
          width: '2048'
        }
      }
    } else if (option === 'daosHW') {
      info = {
        name: 'hW',
        theme: theme,
        background: {
          url: './ndmvr/backgrounds/govorun.jpg',
          radius: '3000',
          height: '2048',
          yPosition: '0',
          width: '2048'
        }
      }
    } else if (option === 'daosHSpeedAvg') {
      info = {
        name: 'hSpeedAvg',
        theme: theme,
        background: {
          url: './ndmvr/backgrounds/govorun.jpg',
          radius: '3000',
          height: '2048',
          yPosition: '0',
          width: '2048'
        }
      }
    } else if (option === 'daosPhysics') {
      info = {
        name: 'daosPhysics',
        theme: theme,
        background: {
          url: './ndmvr/backgrounds/govorun.jpg',
          radius: '3000',
          height: '2048',
          yPosition: '0',
          width: '2048'
        }
      }
    } else {
      info = {
        name: 'Cern',
        theme: theme,
        background: {
          url: './ndmvr/backgrounds/background1.jpg',
          radius: '3000',
          height: '2048',
          yPosition: '1000',
          width: '2048'
        }
      }
    }

    return info
  }

  const renderJsrootHistogram = () => {
    console.log("histo")
    if (option === 'gitlab') {
      return (
        <JSrootHistogram histogram={histograms.gitlab} projectionAxes={['X','Y']} projections={null} projIndex={0}/>
      )
    } else if (option === 'physics') {
      return (
        <JSrootHistogram histogram={histograms.physics} projectionAxes={['X','Y']} projections={histograms.projections} projIndex={0}/>
      )
    } else if (option === 'daosHTime') {
      return (
        <JSrootHistogram histogram={histograms.daosHTime} projectionAxes={['X','Y']} projections={null} projIndex={0}/>
      )
    } else if (option === 'daosHW') {
      return (
        <JSrootHistogram histogram={histograms.daosHW} projectionAxes={['X','Y']} projections={null} projIndex={0}/>
      )
    } else if (option === 'daosHSpeedAvg') {
      return (
        <JSrootHistogram histogram={histograms.daosHSpeedAvg} projectionAxes={['X','Y']} projections={null} projIndex={0}/>
      )
    } else if (option === 'daosPhysics') {
      return (
        <JSrootHistogram histogram={histograms.daosPhysics} projectionAxes={['X','Y']} projections={histograms.daosProjections} projIndex={0}/>
      )
    } else {
      return (
        <JSrootHistogram histogram={histograms.cern} projectionAxes={['X','Y']} projections={null} projIndex={0}/>
      )
    }
  }

  //open file
  useEffect(() => {
    const jsroot = window.JSROOT
      // const filename = https://eos.ndmspc.io/eos/ndmspc/scratch/test/gitlab-hist.root hUsersVsProjects
    if (file === null) {
      const files = {}
      jsroot.openFile('https://root.cern.ch/js/files/hsimple.root')
        .then(file => {
          files.cern = file
          return jsroot.openFile('https://eos.ndmspc.io/eos/ndmspc/scratch/test/test-results-final-out.root')
        })
        .then(file => {
          files.physics = file
          return jsroot.openFile('https://eos.ndmspc.io/eos/ndmspc/scratch/daos/test/daos_phys.root')
        })
        .then(file => {
          console.log(file)
          files.daosPhysics = file
          return jsroot.openFile('https://eos.ndmspc.io/eos/ndmspc/scratch/test/gitlab-hist.root')
        })
        .then(file => {
          files.gitlab = file
          return jsroot.openFile('https://eos.ndmspc.io//eos/ndmspc/scratch/daos/test/daos.root')
        })
        .then(file => {
          files.daos = file
          setFile(files)
        })
        .catch(error => {
          console.log(error)
        })
    }
  }, [file])


  useEffect(() => {
    if (file !== null) {
      const histoObjects = {}
        file.gitlab.readObject('hUsersVsProjects')
          .then(obj => {
            histoObjects.gitlab = obj
            return file.physics.readObject('main')
          })
          .then(obj => {
            histoObjects.physics = obj
            return file.physics.readObject('root')
          })
          .then(obj => {
            histoObjects.projections = obj
            return file.cern.readObject('hpxpy')
          })
          .then(obj => {
            histoObjects.cern = obj
            return file.daos.readObject('hTime')
          })
          .then(obj => {
            histoObjects.daosHTime = obj
            return file.daosPhysics.readObject('main')
          })
          .then(obj => {
            histoObjects.daosPhysics = obj
            return file.daosPhysics.readObject('root')
          })
          .then(obj => {
            histoObjects.daosProjections = obj
            return file.daos.readObject('hW')
          })
          .then(obj => {
            histoObjects.daosHW = obj
            return file.daos.readObject('hSpeedAvg')
          })
          .then(obj => {
            histoObjects.daosHSpeedAvg = obj
          })
          .then(() => {
            setHistograms(histoObjects)
            setHistogramLoaded(true)
            console.log('drawing completed')
          })
          .catch(error => {
            console.log(error)
          })
      }
  }, [file])

  // const handleSubscription = (data) => {
  //  setSelectedBins([...selectedBins, data])
  //  console.log(data)
  // }

  // subscription after render histogram
  useEffect(() => {
    let subscription = binDataDistributor.getBinDistributor().subscribe((data) => {
      if (!focusedBin.id) setFocusedBin(data)
      setSelectedBins((prevState) => [...prevState, data])
    })
    return () => subscription.unsubscribe()
  }, [focusedBin.id])

  // useEffect(() => {
    //setSelectedBins(ndmVrStorage.getBinsFromLocalStorage())
  // }, [])

  return (
    <React.Fragment>
      {/*<div className={classes.gridRow}>
      <GridList className={(show)?classes.gridList:classes.hiddenElm} cols={1}>
        {selectedBins.map(bin => (
          <Card className={classes.card} key={bin.id}>
            <CardContent>
              <Typography className={classes.cardTitle} color="textSecondary" gutterBottom>
                {`content: ${bin.absoluteContent}`}
              </Typography>
              <Typography className={classes.pos} color="textSecondary">
                {`[${bin.xMin},${bin.xMax}] [${bin.yMin},${bin.yMax}] [${bin.zMin},${bin.zMax}]`}
              </Typography>
              <LinearProgress variant="determinate" value={58} />
              <Typography variant="body2" component="p">
                {`Progress 58%`}
              </Typography>
            </CardContent>
            <CardActions disableSpacing>
              <IconButton aria-label={`star Title`}>
                <StarBorderIcon className={classes.title}/>
              </IconButton>
            </CardActions>
          </Card>
        ))
        }
      </GridList>
      </div>*/}
      <div className={classes.root}>
        <Grid container>
          <Grid item xs={12} md={7}>
            {(histograms !== null) ?
               <NdmVrScene data={getData()} info={getInfo()} />
            :
              <CircularProgress size={70} className={classes.loadProgress}/>
            }
          </Grid>
          <Slide direction="left" in={true} timeout={{ enter: 1500}}>
            <Grid item xs={12} md={5} className={classes.dataSection}>
              <FormControl variant="outlined" className={classes.formControl}>
                <InputLabel htmlFor="outlined-projection-native-simple">Data</InputLabel>
                <Select
                  native
                  onChange={handleChange}
                  label="Data"
                  inputProps={{
                    name: 'Data',
                    id: 'outlined-projection-native-simple',
                  }}
                >
                  <option value='gitlab' key={0}>Gitlab data</option>
                  <option value='physics' key={1}>Physics data</option>
                  <option value='cern' key={2}>Cern demo</option>
                  <option value='daosPhysics' key={3}>daosPhysics</option>
                  <option value='daosHTime' key={4}>daosHTime</option>
                  <option value='daosHW' key={5}>daosHW</option>
                  <option value='daosHSpeedAvg' key={6}>daosHSpeedAvg</option>
                </Select>
              </FormControl>
              {expanded ?
                (
                  <>
                    <Container className={classes.optionsContainer}>
                      <FormControl variant="outlined" className={classes.formControl}>
                        <InputLabel htmlFor="outlined-theme-native-simple">Theme color</InputLabel>
                        <Select
                          native
                          value={theme}
                          onChange={handleThemeChange}
                          label="Theme color"
                          inputProps={{
                            name: 'theme',
                            id: 'outlined-theme-native-simple',
                          }}
                        >
                          <option value={'def'} key={10}>def</option>
                          <option value={'fire'} key={11}>fire</option>
                          <option value={'nebula'} key={12}>nebula</option>
                          <option value={'rainforest'} key={13}>rainforest</option>
                          <option value={'nature'} key={14}>nature</option>
                          <option value={'water'} key={15}>water</option>
                          <option value={'sunflower'} key={16}>sunflower</option>
                          <option value={'blood'} key={17}>blood</option>
                          <option value={'magma'} key={18}>magma</option>
                          <option value={'jupiter'} key={19}>jupiter</option>
                          <option value={'magic'} key={20}>magic</option>
                        </Select>
                      </FormControl>
                      <FormControl variant="outlined" className={classes.formControl}>
                        <InputLabel htmlFor="outlined-theme-native-simple">Section range</InputLabel>
                        <Select
                          native
                          value={range}
                          onChange={handleRangeChange}
                          label="Theme color"
                          inputProps={{
                            name: 'theme',
                            id: 'outlined-theme-native-simple',
                          }}
                        >
                          <option value='3' key={10}>4</option>
                          <option value='4' key={11}>5</option>
                          <option value='5' key={12}>6</option>
                          <option value='6' key={13}>7</option>
                          <option value='7' key={14}>8</option>
                          <option value='8' key={15}>9</option>
                          <option value='9' key={16}>10</option>
                          <option value='10' key={17}>11</option>
                          <option value='11' key={18}>12</option>
                          <option value='12' key={19}>13</option>
                          <option value='13' key={20}>14</option>
                          <option value='20' key={21}>20</option>
                          <option value='30' key={22}>30</option>
                          <option value='50' key={23}>50</option>
                          <option value='100' key={24}>100</option>
                        </Select>
                      </FormControl>
                      {selectedBins.length > 0 && <><FormControl variant="outlined" className={classes.formControl}>
                        <InputLabel htmlFor="outlined-theme-native-simple">Selected bins</InputLabel>
                        <Select
                          native
                          onChange={handleBinFocusChange}
                          label="Theme color"
                          inputProps={{
                            name: 'theme',
                            id: 'outlined-theme-native-simple',
                          }}
                        >
                          {selectedBins.map((bin) => <option value={JSON.stringify(bin)} key={bin.id}>{bin.id}</option>)}
                        </Select>
                      </FormControl>
                      {focusedBin.id && <>
                      <Button className={classes.showDialog} variant="outlined" onClick={handleClickOpen}>
                        Display focused bin
                      </Button>
                      <Dialog
                        open={openDia}
                        onClose={handleClose}
                        aria-labelledby="draggable-dialog-title"
                      >
                        <DialogTitle style={{ cursor: 'move',margin: 'auto' }} id="draggable-dialog-title">
                          {focusedBin.id}
                        </DialogTitle>
                        <DialogContent>
                          <DialogContentText>
                            {JSON.stringify(focusedBin)}
                          </DialogContentText>
                        </DialogContent>
                        <DialogActions>
                          <Button style={{ margin: 'auto' }} autoFocus onClick={handleClose}>
                            Cancel
                          </Button>
                        </DialogActions>
                      </Dialog></>}
                      </>
                      }
                    </Container>
                    <Button  className={classes.showOptions} onClick={() => setExpanded(prevState => prevState === true ? false : true)}>Hide options</Button>
                  </>
                )
              :
                (<Button className={classes.showOptions} onClick={() => setExpanded(prevState => prevState === false ? true : false)}>Show options</Button>)
              }
              {(histogramLoaded)?
                renderJsrootHistogram()
                :
                <CircularProgress size={70} className={classes.circularProgress}/>
              }
            </Grid>
          </Slide>
        </Grid>
      </div>
    </React.Fragment>
  )
}

export default THProjection
