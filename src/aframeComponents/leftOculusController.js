// component for modification aframe entity of histogram
// manage functionality of camera
import AFRAME from 'aframe'
import {
  oculusXButtonDownFunction,
  oculusYButtonDownFunction,
  oculusUpdateCameraReference,
  oculusThumbStickPredefinedCameraPositionWithOffset,
  oculusThumbStickForMoving,
  oculusSwitchViewWithBanners,
  oculusChangeBannerContent
} from '../controllers/oculusController'
/**
 * A-Frame interakčný komponent zabezpečí možnosť používateľa interagovať s ľavým ovládačom zariadenia Oculus.
 * @component left-controller-logging
 * @module leftOculusAframeComponent
 */

// aframe component for positioning histogram in scene
export default AFRAME.registerComponent('left-controller-logging', {
  init: function () {
    let gripActive = false
    let speed = 1
    const el = this.el
    // oculus listeners
    el.addEventListener('gripdown', () => {
      gripActive = true
    })
    el.addEventListener('gripup', () => {
      gripActive = false
    })
    el.addEventListener('thumbstickdown', () => {
      if (gripActive) {
        speed++
        if (speed > 20) speed = 20
      } else {
        speed--
        if (speed < 1) speed = 1
      }
    })
    el.addEventListener('thumbstickmoved', (event) => {
      if (gripActive) {
        oculusThumbStickPredefinedCameraPositionWithOffset(event)
      } else {
        oculusThumbStickForMoving(event, speed)
      }
    })
    el.addEventListener('ybuttondown', () => {
      if (gripActive) {
        oculusXButtonDownFunction(speed)
      } else {
        oculusSwitchViewWithBanners()
      }
    })
    el.addEventListener('xbuttondown', () => {
      if (gripActive) {
        oculusYButtonDownFunction(speed)
      } else {
        oculusChangeBannerContent()
      }
    })
  },

  update: function () {
    oculusUpdateCameraReference()
  }
})
