// component to insert data to th2i bin
import AFRAME from 'aframe'
/**
 * A-Frame interakčný komponent pridávajúci funkcionalitu entite binu TH2 histogram, ktorá umožňuje zmenu atribútov entity.
 * @component binth2
 * @param {string} id - Identifikátor binu
 * @param {number} content - Hodnota optimalizovaného obsahu binu (predvolená 0)
 * @param {number} absoluteContent - Hodnota neoptimalizovaného obsahu binu (predvolená 0)
 * @param {string} binName - Meno binu
 * @param {string} xTitle - Titulok na osi x
 * @param {string} yTitle - Titulok na osi y
 * @param {number} xMin - Súradnica min binu na osi x
 * @param {number} yMin - Súradnica min binu na osi y
 * @param {number} xMax - Súradnica max binu na osi x
 * @param {number} yMax - Súradnica max binu na osi y
 * @param {number} xCenter - Súradnica stredu binu na osi x
 * @param {number} yCenter - Súradnica stredu binu na osi y
 * @param {number} xWidth - Šírka binu na osi x
 * @param {number} yWidth - Šírka binu na osi y
 * @param {string} color - Farba binu
 * @param {string} axisX - Farba označení na osi x
 * @param {string} axisY - Farba označení na osi y
 * @param {string} axisZ - Farba označení na osi z
 * @param {string} selectColor - Farba určená za zvolenie binu
 * @param {string} markedColor - Farba určená pre označenie binu (v poli označených binov)
 * @module th2AframeComponent
 */

// aframe component for positioning histogram in scene
AFRAME.registerComponent('binth2', {
  schema: {
    id: { type: 'string' },
    content: { default: 0 },
    absoluteContent: { default: 0 },
    binName: { default: '' },
    xTitle: { type: 'string' },
    yTitle: { type: 'string' },
    xMin: { type: 'number' },
    yMin: { type: 'number' },
    xMax: { type: 'number' },
    yMax: { type: 'number' },
    xCenter: { type: 'number' },
    yCenter: { type: 'number' },
    xWidth: { type: 'number' },
    yWidth: { type: 'number' },
    color: { type: 'string' },
    axisX: { type: 'string' },
    axisY: { type: 'string' },
    axisZ: { type: 'string' },
    selectColor: { type: 'string' },
    markedColor: { type: 'string' }
  },

  init: function () {
    const el = this.el // Entity.
    el.userData = {
      typeName: 'TH2',
      id: this.data.id,
      content: this.data.content,
      absoluteContent: this.data.absoluteContent,
      binName: this.data.binName,
      xTitle: this.data.xTitle,
      yTitle: this.data.yTitle,
      xMin: this.data.xMin,
      yMin: this.data.yMin,
      xMax: this.data.xMax,
      yMax: this.data.yMax,
      xCenter: this.data.xCenter,
      yCenter: this.data.yCenter,
      xWidth: this.data.xWidth,
      yWidth: this.data.yWidth,
      color: this.data.color,
      axisX: this.data.axisX,
      axisY: this.data.axisY,
      axisZ: this.data.axisZ,
      selectColor: this.data.selectColor,
      markedColor: this.data.markedColor
    }
  },
  update: function () {
    const el = this.el // Entity.
    el.userData = {
      typeName: 'TH2',
      id: this.data.id,
      content: this.data.content,
      absoluteContent: this.data.absoluteContent,
      binName: this.data.binName,
      xTitle: this.data.xTitle,
      yTitle: this.data.yTitle,
      xMin: this.data.xMin,
      yMin: this.data.yMin,
      xMax: this.data.xMax,
      yMax: this.data.yMax,
      xCenter: this.data.xCenter,
      yCenter: this.data.yCenter,
      xWidth: this.data.xWidth,
      yWidth: this.data.yWidth,
      color: this.data.color,
      axisX: this.data.axisX,
      axisY: this.data.axisY,
      axisZ: this.data.axisZ,
      selectColor: this.data.selectColor,
      markedColor: this.data.markedColor
    }
  }
})
