import React, { useState, useEffect } from 'react'
import jsrootSubject from '../../observables/jsrootSubject'
import jsrootService from '../../services/jsrootService'

// styles
const styles = {
  box: {
    display: 'block',
    margin: 'auto',
    width: '90%',
    textAlign: 'center',
    align: 'center',
    paddingTop: 2
  },
  projection: {
    display: 'block',
    margin: 'auto',
    width: '460px',
    height: '380px'
  },
  form: {
    display: 'block',
    width: '60%',
    margin: 'auto',
    paddingTop: '2%',
    paddingBottom: '2%'
  },
  label: {
    display: 'block'
  },
  input: {
    display: 'block',
    width: '85%',
    margin: 'auto',
    height: 40,
    border: '1px solid grey',
    borderRadius: 3,
    textAlign: 'center',
    fontSize: 20,
    color: 'black',
    opacity: 0.8
  }
}

/**
 * React komponent pre zobrazenie bočného panelu s JSROOT projekciami.
 * Zobrazenie SVG elementu s histogramom a potrebným formulárom pre nastavenie atribútov.
 * @component
 * @param {Object} histogram - Objekt histogramu, z ktorého je potrebné spraviť projekciu
 * @param {Array} projectionAxes - Pole obsahuje názvy osí, podľa ktorých sa má vytvoriť projekcia
 * @param {Object} projections - Objekt obsahujúci súborovú štruktúru s projekciami
 * @param {Number} projIndex - Index projekcie v poli, ktorá sa má zobraziť
 * @example
 * const histogram = {
 *   gitlab: // vytvorený alebo prečítany z root súboru JSROOT-tom
 *   projections: // prečítaná z root súboru JSROOT-tom
 * }
 * return (
 *   <JSrootHistogram
 *      histogram={histogram.gitlab}
 *      projectionAxes={['X','Y']}
 *      projections={histogram.projections}
 *      projIndex={0}/>
 * )
 */
const JsrootHistogram = ({
  histogram,
  projectionAxes,
  projections,
  projIndex
}) => {
  let subscription
  const [axis, setAxis] = useState('X')
  const [selectedBin, setSelectedBin] = useState(null)

  // const histogram = props.histogram object of histogram in props
  const handleInputChange = (event) => {
    setAxis(event.target.value)
  }

  // handle subscription
  const handleSubscription = (data) => {
    setSelectedBin(data)
  }

  const renderForm = () => {
    return (
      <div className='form-group' style={styles.form}>
        <label htmlFor='formGroupExampleInput' style={styles.label}>
          Projection axis
        </label>
        <select
          onChange={handleInputChange}
          className='form-control'
          id='axisProjection'
          style={styles.input}
        >
          <option value={projectionAxes[0]}>
            {histogram[`f${projectionAxes[0]}axis`].fTitle
              ? histogram[`f${projectionAxes[0]}axis`].fTitle
              : projectionAxes[0]}
          </option>
          <option value={projectionAxes[1]}>
            {histogram[`f${projectionAxes[1]}axis`].fTitle
              ? histogram[`f${projectionAxes[1]}axis`].fTitle
              : projectionAxes[1]}
          </option>
        </select>
      </div>
    )
  }

  const createAndDisplayProjection = () => {
    if (projections !== null) {
      jsrootService.openTH1Projection(
        axis,
        { obj: histogram, bin: selectedBin },
        'projectionContainer',
        projectionAxes,
        projections,
        projIndex
      )
    } else {
      jsrootService.createTH1Projection(
        axis,
        { obj: histogram, bin: selectedBin },
        'projectionContainer',
        projectionAxes
      )
      setTimeout(() => {
        jsrootService.displayImageOfProjection(
          'projectionContainer',
          'th-mapping',
          '500px',
          '500px'
        )
      }, 500)
    }
  }

  // subscribe for bin data
  useEffect(() => {
    jsrootService.jsrootLibrary = window.JSROOT
    setSelectedBin(null)

    subscription = jsrootSubject.getServiceEvent().subscribe(handleSubscription)
    // console.log(histogram)
    return () => subscription.unsubscribe()
  }, [histogram])

  // update projection
  useEffect(() => {
    if (selectedBin != null) createAndDisplayProjection()
  }, [axis, selectedBin, projections])

  return (
    <div style={styles.box}>
      {projections === null && selectedBin !== null && renderForm()}
      <div id='projectionContainer' style={styles.projection} />
    </div>
  )
}

export default JsrootHistogram
