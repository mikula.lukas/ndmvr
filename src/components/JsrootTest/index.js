import { JSROOTExample } from '@ndmspc/react-jsroot'
import React from 'react'

const styles = {
  container: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  },
  item: {
    width: '90%',
    margin: 'auto',
    minHeigth: '60%'
  },
  test: {
    minHeigth: '100px !important'
  }
}

const JsrootTest = () => {
  // console.log(JSROOT.version)
  return (
    <div style={styles.container}>
      <div style={styles.item}>
        <JSROOTExample name='hist1' />
      </div>
    </div>
  )
}

export default JsrootTest
