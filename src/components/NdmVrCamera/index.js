import React, { useState, useEffect } from 'react'
import 'aframe'
import cameraSubject from '../../observables/cameraSubject'
import jsrootService from '../../services/jsrootService'

/**
 * React komponent pre vytvorenie kamery a jej menežment.
 * @component
 * @example
 * return (
 *  <NdmVrCamera />
 * )
 */
const NdmVrCamera = () => {
  let subscription
  const offset = 30
  const [rotation, setRotation] = useState(0)
  const [prevPosition, setPrevPosition] = useState(null)
  const [inputDevice, setInputDevice] = useState('keyboard')
  const [show, setShow] = useState(false)
  const [showProjection, setShowProjection] = useState(false)

  const handleSubscription = (data) => {
    if (data.device) {
      if (inputDevice !== data.device) setInputDevice(data.device)
      setShow(!show)
    } else if (data === 'shift') {
      compileRotation()
    } else if (data === 'show') {
      setShowProjection(!showProjection)
    }
  }

  const compileRotation = () => {
    let currentRotation = rotation + 90
    if (currentRotation >= 360) currentRotation = 0
    setRotation(currentRotation)
  }

  // render banners for projection
  const renderProjectionsBanners = () => {
    return (
      <a-entity
        position={
          prevPosition === null
            ? `0 ${offset} 0`
            : `${prevPosition.x} ${offset} ${prevPosition.z}`
        }
        animation__rotation={`property: rotation; to: 0 ${rotation} 0; dur: 1000; easing: linear;`}
        material='color: white'
      >
        <a-entity
          // geometry='primitive: box; width: 18; height: 5; depth: 0.5;'
          /* geometry={
            inputDevice === 'oculus'
              ? 'primitive: box; width: 40; height: 10; depth: 0.1;'
              : 'primitive: box; width: 18; height: 5; depth: 0.1;'
          } */
          geometry='primitive: box; width: 50; height: 10; depth: 0.1;'
          // position={`8 ${inputDevice === 'oculus' ? 3 - offset : 3} -5`}
          // position={inputDevice === 'oculus' ? '-30 28 0' : '8 3 -5'}
          position='-30 28 0'
          // rotation='0 -45 0'
          // rotation={inputDevice === 'oculus' ? '45 90 0' : '0 -45 0'}
          rotation='45 90 0'
          material='color:white;'
        >
          <a-text
            color='black'
            // width='20'
            // width={inputDevice === 'oculus' ? '50' : '20'}
            width={50}
            value={
              inputDevice === 'keyboard'
                ? 'Press C to show help panels'
                : 'Press Button X to show help panels'
            }
            // position={inputDevice === 'oculus' ? '-18 0 1' : '-8 0 1'}
            position='-20 2.8 1'
          />
          <a-text
            color='black'
            // width='20'
            // width={inputDevice === 'oculus' ? '50' : '20'}
            width={50}
            value={
              inputDevice === 'keyboard'
                ? 'Press V to switch to histogram view'
                : 'Press Button Y to switch to histogram view'
            }
            // position='-8 1 1'
            // position={inputDevice === 'oculus' ? '-18 2.8 1' : '-8 1 1'}
            position='-20 0 1'
          />
          <a-text
            color='black'
            // width='20'
            // width={inputDevice === 'oculus' ? '50' : '20'}
            width={50}
            value={
              inputDevice === 'keyboard'
                ? 'Press X to shift panels'
                : 'Press right Trigger to shift panels'
            }
            // position='-8 -1 1'
            // position={inputDevice === 'oculus' ? '-18 -2.8 1' : '-8 -1 1'}
            position='-20 -2.8 1'
          />
        </a-entity>
        <a-box
          id='bannerId_1'
          scale='80 50 2'
          animation__position='property: position; from: 0 0 -60; to: 0 -1 -42; delay: 0; dur: 800; easing: linear;'
          material='transparent:true'
        />
        <a-box
          scale='80 52 2'
          animation__position='property: position; from: 0 0 -60; to: 0 0 -42.5; delay: 0; dur: 800; easing: linear;'
          material='color: white;'
        />
        <a-box
          id='bannerId_2'
          scale='80 50 2'
          animation__position='property: position; from: 60 0 0; to: 42 -1 0; delay: 200; dur: 800; easing: linear;'
          rotation='0 90 0'
          material='transparent:true'
        />
        <a-box
          scale='80 52 2'
          animation__position='property: position; from: 60 0 0; to: 42.5 0 0; delay: 200; dur: 800; easing: linear;'
          rotation='0 90 0'
          material='color: white;'
        />
        <a-box
          id='bannerId_3'
          scale='80 50 2'
          animation__position='property: position; from: 0 0 60; to: 0 -1 42; delay: 400; dur: 800; easing: linear;'
          material='transparent:true'
        />
        <a-box
          scale='80 52 2'
          animation__position='property: position; from: 0 0 60; to: 0 0 42.5; delay: 400; dur: 800; easing: linear;'
          material='color: white;'
        />
        <a-box
          id='bannerId_4'
          scale='80 50 2'
          animation__position='property: position; from: -60 0 0; to: -42 -1 0; delay: 600; dur: 800; easing: linear;'
          rotation='0 90 0'
          material='transparent:true'
        />
        <a-box
          scale='80 52 2'
          animation__position='property: position; from: -60 0 0; to: -42.5 0 0; delay: 600; dur: 800; easing: linear;'
          rotation='0 90 0'
          material='color: white;'
        />
      </a-entity>
    )
  }

  // render banners for controls
  const renderBannersWithControls = () => {
    if (inputDevice === 'keyboard') {
      return (
        <a-entity
          position={
            prevPosition === null
              ? `0 ${offset} 0`
              : `${prevPosition.x} ${offset} ${prevPosition.z}`
          }
          animation__rotation={`property: rotation; to: 0 ${rotation} 0; dur: 1000; easing: linear;`}
          material='color: white'
        >
          <a-entity
            // geometry='primitive: box; width: 15; height: 5; depth: 0.5;'
            // position='8 3 -5'
            // rotation='0 -45 0'
            geometry='primitive: box; width: 50; height: 10; depth: 0.1;'
            position='-30 28 0'
            rotation='45 90 0'
            material='color: white'
          >
            <a-text
              color='black'
              // width='20'
              width='50'
              // value='Press C to show projection panels'
              value={
                showProjection
                  ? 'Press C to show help panels'
                  : 'Press C to show projection panels'
              }
              // position='-6 0 1'
              position='-20 2.8 1'
            />
            <a-text
              color='black'
              // width='20'
              width='50'
              value='Press V to switch to histogram view'
              // position='-6 1 1'
              position='-20 0 1'
            />
            <a-text
              color='black'
              // width='20'
              width='50'
              value='Press X to shift panels'
              // position='-6 -1 1'
              position='-20 -2.8 1'
            />
          </a-entity>
          <a-box
            scale='80 50 2'
            animation__position='property: position; from: -30 0 60; to: -30 0 30; delay: 0; dur: 800; easing: linear;'
            rotation='0 -45 0'
            // src='https://i.imgur.com/x78XYHM.png'
            src='./ndmvr/keyboardControls/keyboard.png'
          />
          <a-box
            scale='80 50 2'
            animation__position='property: position; from: -30 0 -60; to: -30 0 -30; delay: 200; dur: 800; easing: linear;'
            rotation='0 45 0'
            src='./ndmvr/keyboardControls/keyboard1.png'
          />
          <a-box
            scale='80 50 2'
            animation__position='property: position; from: 30 0 -60; to: 30 0 -30; delay: 400; dur: 800; easing: linear;'
            rotation='0 -45 0'
            src='./ndmvr/keyboardControls/keyboard2.png'
          />
          <a-box
            scale='80 50 2'
            animation__position='property: position; from: 30 0 60; to: 30 0 30; delay: 600; dur: 800; easing: linear;'
            rotation=' 0 45 0'
            src='./ndmvr/keyboardControls/keyboard3.png'
          />
        </a-entity>
      )
    } else {
      return (
        <a-entity
          position={
            prevPosition === null
              ? `0 ${offset} 0`
              : `${prevPosition.x} ${offset} ${prevPosition.z}`
          }
          animation__rotation={`property: rotation; to: 0 ${rotation} 0; dur: 1000; easing: linear;`}
        >
          <a-entity
            geometry='primitive: box; width: 50; height: 10; depth: 0.1;'
            position='-30 28 0'
            rotation='45 90 0'
            material='color: white'
          >
            <a-text
              color='black'
              width='50'
              // value='Press Button X to show projection panels'
              value={
                showProjection
                  ? 'Press Button X to show help panels'
                  : 'Press Button X to show projection panels'
              }
              position='-20 2.8 1'
            />
            <a-text
              color='black'
              width='50'
              value='Press Button Y to switch to histogram view'
              position='-20 0 1'
            />
            <a-text
              color='black'
              width='50'
              value='Press right Trigger to shift panels'
              position='-20 -2.8 1'
            />
          </a-entity>
          <a-box
            scale='80 50 2'
            animation__position='property: position; from: -35 0 60; to: -32 0 32; delay: 0; dur: 800; easing: linear;'
            rotation='-10 -45 0'
            src='./ndmvr/oculusControls/oculus.png'
          />
          <a-box
            scale='80 50 2'
            animation__position='property: position; from: -35 0 -60; to: -32 0 -32; delay: 200; dur: 800; easing: linear;'
            rotation='10 45 0'
            src='./ndmvr/oculusControls/oculus2.png'
          />
          <a-box
            scale='80 50 2'
            animation__position='property: position; from: 35 0 -60; to: 32 0 -32; delay: 400; dur: 800; easing: linear;'
            rotation='10 -45 0'
            src='./ndmvr/oculusControls/oculus1.png'
          />
          <a-box
            scale='80 50 2'
            animation__position='property: position; from: 35 0 60; to: 32 0 32; delay: 600; dur: 800; easing: linear;'
            rotation='-10 45 0'
            src='./ndmvr/oculusControls/oculus3.png'
          />
        </a-entity>
      )
    }
  }

  const hideMappingBanner = () => {
    const banner = document.getElementById('th-mapping')
    const background = document.getElementById('th-background')
    if (banner !== undefined && background !== undefined) {
      banner.setAttribute('material', 'opacity', 0)
      background.setAttribute('material', 'opacity', 0)
    }
  }

  const showMappingBanner = () => {
    const banner = document.getElementById('th-mapping')
    const background = document.getElementById('th-background')
    if (banner !== undefined && background !== undefined) {
      banner.setAttribute('material', 'opacity', 1)
      background.setAttribute('material', 'opacity', 1)
    }
  }

  useEffect(() => {
    subscription = cameraSubject
      .getCameraSubject()
      .subscribe(handleSubscription)

    return () => subscription.unsubscribe()
  }, [show, showProjection, rotation])

  useEffect(() => {
    const camera = document.getElementById('camera')
    if (!show) {
      camera.object3D.position.y = 1.6
      if (prevPosition !== null) {
        camera.object3D.position.x = prevPosition.x
        camera.object3D.position.z = prevPosition.z
        camera.object3D.position.y = prevPosition.y
      }
      showMappingBanner()
    } else {
      setPrevPosition({
        x: camera.object3D.position.x,
        y: camera.object3D.position.y,
        z: camera.object3D.position.z
      })
      camera.object3D.position.y = camera.object3D.position.y + offset
      hideMappingBanner()
    }
  }, [show, inputDevice])

  useEffect(() => {
    if (showProjection) {
      jsrootService.displayImageOfProjection(
        'projectionContainer',
        'bannerId_1',
        '500px',
        '400px'
      )
    }
  }, [showProjection])

  return (
    <a-entity id='cameraWrapper' rotation='0 0 0' position='0 2 0'>
      {show && !showProjection && renderBannersWithControls()}
      {show && showProjection && renderProjectionsBanners()}
      <a-camera
        id='camera'
        look-control
        wasd-controls-enabled='acceleration:=50'
      ></a-camera>
        {/* <a-entity
          cursor='fuse: false; fuseTimeout: 2000;'
          raycaster='objects: .clickable; showLine: false; far: 100;'
          animation__click='property: scale; startEvents: click; from: 0.1 0.1 0.1; to: 1.4 1.4 1.4; dur: 150'
          animation__mouseenter='property: scale; startEvents: mouseenter; from: 1 1 1; to: 1.4 1.4 1.4; dur: 180'
          animation__mouseleave='property: scale; startEvents: mouseleave; from: 1.4 1.4 1.4; to: 1 1 1; dur: 180'
          line='color: orange; opacity: 0.5'
          far='100'
          position='0 0 -1'
          geometry='primitive: ring; radiusInner: 0.02; radiusOuter: 0.03'
          material='color: white; shader: flat'
        /> */}
        <a-entity laser-controls="hand: left" 
     
        raycaster="objects: .clickable; 
        lineOpacity: 0.5; far: 5;"></a-entity>
        <a-entity oculus-touch-controls='hand: left' left-controller-logging />
        <a-entity
          oculus-touch-controls='hand: right'
          right-controller-logging
        />
      
    </a-entity>
  )
}

export default NdmVrCamera
