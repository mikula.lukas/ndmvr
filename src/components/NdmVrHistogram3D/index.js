import React, { useState, useEffect } from 'react'
import 'aframe'
import '../../aframeComponents/cursorEventAframeComponent'
import '../../aframeComponents/thAframeComponent'
import '../../aframeComponents/histogramAframeComponent'
import '../../aframeComponents/labelHandlerAframeComponent'
import HistogramReactFactory from '../../utils/histogramGenerator'
// import { CameraService } from '../../services/cameraService'
import {
  histogramTH2Service,
  histogramTH3Service
} from '../../observables/histogramSubject'

/**
 * React komponent pre vytvorenie histogramu.
 * @component
 * @param {string} name - Názov pre histogram
 * @param {Object} histogram - Objekt histogramu, ktorý sa bude v komponente vizualizovať
 * @param {Object} histoSection - Objekt obsahujúci údaje o sekcií a rozsahu, ktorý bude zobrazený
 * @param {Object} projections - Objekt obsahujúci súborovú štruktúru s projekciami
 * @param {number} range - Definuje rozsah, v prípade ak sa vizualizuje bez určených ofsetov iba určitý rozsah
 * @param {string} theme - Definuje tému pre vizualizáciu histogramu
 * @example
 * return (
 *   <NdmVrHistogram3D
 *    name={info.name}
 *    histogram={data.histogram}
 *    histoSection={data.section}
 *    projections={data.projections}
 *    range={data.range}
 *    theme={info.theme}/>
 * )
 */
const NdmVrHistogram3D = ({
  name,
  histogram,
  histoSection,
  projections,
  range,
  theme
}) => {
  // const unmounted = useRef(false)
  // pressed keys
  let subscription
  // const cameraService = new CameraService()
  // create factory
  const [factory] = useState(
    new HistogramReactFactory(
      name,
      histogram,
      histoSection || null,
      range,
      projections,
      theme || 'def'
    )
  )
  const [mode, setMode] = useState('default')
  const [elements, setElements] = useState(factory.getElements(mode))

  // handler functions
  // handle subscription
  const handleSubscription = (data) => {
    if (histogram._typename.includes(data.name)) {
      // save new section to local storage
      if (data.axis) {
        factory.updateSection(data.axis, data.increment, data.defaultRange)
        setElements(factory.getElements(mode))
      } else {
        setMode((prevState) =>
          prevState !== data.fFunction ? data.fFunction : prevState
        )
      }
    }
  }

  // use effect hooks
  // update histogram unique name
  useEffect(() => {
    factory.setUniqueName(name)
    setElements(factory.getElements(mode))
  }, [name])

  // update histogram and projection
  useEffect(() => {
    factory.setHistogram(histogram, projections)
    setElements(factory.getElements(mode))
  }, [histogram, projections])

  // update histogram section
  useEffect(() => {
    factory.setSection(histoSection)
    setElements(factory.getElements(mode))
  }, [histoSection])

  // update histogram section range
  useEffect(() => {
    factory.setRange(range)
    setElements(factory.getElements(mode))
  }, [range])

  // update histogram unique name
  useEffect(() => {
    factory.setTheme(theme)
    setElements(factory.getElements(mode))
  }, [theme])

  // update histogram with certain mode
  useEffect(() => {
    setElements(factory.getElements(mode))
  }, [mode])

  // subscription after render histogram
  useEffect(() => {
    if (histogram._typename.includes('TH2')) {
      subscription = histogramTH2Service
        .getChangedSection()
        .subscribe(handleSubscription)
    } else if (histogram._typename.includes('TH3')) {
      subscription = histogramTH3Service
        .getChangedSection()
        .subscribe(handleSubscription)
    }
    return () => subscription.unsubscribe()
  }, [elements])

  if (elements?.bins) {
    return (
      <a-entity histogram-control>
        {elements.bins.map((bin) => (
          <a-box
            key={bin.id}
            class='clickable'
            material={`color: ${bin.color}; opacity: 0.75;`}
            binth={bin.binData}
            mouseevent
            animation={`property: scale; from: ${bin.animation.fromWidth} ${bin.animation.fromHeight} ${bin.animation.fromDepth}; to: ${bin.animation.toWidth} ${bin.animation.toHeight} ${bin.animation.toDepth}; delay:100; dur: 1500; easing: linear;`}
            animation__1='property: material.opacity; from: 0; to: 0.75; dur: 2000;'
            animation__2={`property: position; from: ${bin.animation2.fromX} ${bin.animation2.fromY} ${bin.animation2.fromZ}; to: ${bin.animation2.toX} ${bin.animation2.toY} ${bin.animation2.toZ}; dur: 1000;`}
            animation__3={`property: material.color; from: ${bin.animation3.fromColor}; to: ${bin.animation3.toColor}; dur: 1600;`}
          />
        ))}

        {elements.labels.map((label) => (
          <a-entity
            key={label.key}
            geometry={`primitive: ${label.geometry.primitive}; width:${label.geometry.width}; height:${label.geometry.height};`}
            text={`color: ${label.text.color}; width: ${label.text.width}; height: ${label.text.height}; align:center;`}
            label-handler={`value: ${label.labelHandler.value}; delay:${label.labelHandler.delay}`}
            rotation={`${label.rotation.x} ${label.rotation.y} ${label.rotation.z}`}
            material={`color: ${label.material.color};`}
            animation='property: text.width; from: 0; to: 4; dur: 1000;'
            animation__1='property: material.opacity; from: 0; to: 0.8; dur: 1500;'
            animation__2={`property: position; from: ${label.animation.fromX} ${label.animation.fromY} ${label.animation.fromZ}; to: ${label.animation.toX} ${label.animation.toY} ${label.animation.toZ}; dur: 1005;`}
          />
        ))}
        {elements.titles.map((title) => (
          <a-entity
            key={title.key}
            text={`value: ${title.text.value}; color: ${title.text.color}; width: 230; height:auto; align:center;`}
            rotation={`${title.rotation.x} ${title.rotation.y} ${title.rotation.z}`}
            position={`${title.position.x} ${title.position.y} ${title.position.z}`}
          >
            <a-text
              value={title.innerText.value}
              position='0 10 0'
              width='150'
              height='auto'
              align='center'
              color={title.innerText.color}
            />
            <a-entity
              geometry='primitive: plane; width:170; height:30;'
              position='0 0 -1'
              material='opacity: 0.9; color: white;'
            />
          </a-entity>
        ))}
        {elements.axisLabels.map((axisLabel) => (
          <a-entity
            id={axisLabel.id}
            key={axisLabel.key}
            text={`value: ${axisLabel.text.value}; color: ${axisLabel.text.color}; width: 10; height:auto; align:center;`}
            rotation={`${axisLabel.rotation.x} ${axisLabel.rotation.y} ${axisLabel.rotation.z}`}
            animation={`property: position; from: ${axisLabel.animation.fromX} ${axisLabel.animation.fromY} ${axisLabel.animation.fromZ}; to: ${axisLabel.animation.toX} ${axisLabel.animation.toY} ${axisLabel.animation.toZ}; dur: 1000;`}
            material='opacity: 0;'
          />
        ))}
        {elements.ground && (
          <a-box
            key={elements.ground.key}
            width={elements.ground.scale.width}
            material='color: #171717'
            height={elements.ground.scale.height}
            depth={elements.ground.scale.depth}
            animation={`property: position; from: ${elements.ground.animation.fromX} ${elements.ground.animation.fromY} ${elements.ground.animation.fromZ};
         to:
         ${elements.ground.animation.toX} ${elements.ground.animation.toY} ${elements.ground.animation.toZ}; dur: 1000;`}
          />
        )}
        {elements.banners.map((banner) => (
          <a-box
            key={banner.key}
            id={banner.id}
            geometry={`width:${banner.geometry.width}; height:${banner.geometry.height}; depth:${banner.geometry.depth};`}
            position={`${banner.position.x} ${banner.position.y} ${banner.position.z}`}
            material={`color: ${banner.material.color}, transparent: ${banner.material.transparent}`}
          />
        ))}
      </a-entity>
    )
  } else {
    return (
      <a-box
        animation__1='property: rotation; to: 0 180 360; dur: 10000; easing: linear; loop: true'
        animation__2='property: scale; from: 1 1 1; to: 10 10 10; dur: 15000; timeout: 15000; loop: true; easing: linear; dir: alternate'
        animation__color='property: material.color; from: #003682; to: #7d0002; dur: 80000: easing: linear: loop: true; dir: alternate'
        position='0 1.5 -2'
      />
    )
  }
}

export default NdmVrHistogram3D
