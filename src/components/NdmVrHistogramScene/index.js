import React from 'react'
import NdmVrCamera from '../NdmVrCamera'
import NdmVrHistogram3D from '../NdmVrHistogram3D'

/**
 * React komponent pre vytvorenie VR scény s všetkými potrebnými komponentami.
 * @component
 * @param {Object} data - Objekt obsahuje objekty histogramu, sekcie poprípade projekcie
 * @param {Object} info - Objekt obsahuje sekundárne údaje (meno, témy, atribúty pozadia v scéne)
 * @example
 * data = {
 *    histogram: histograms.physics,
 *    projections: histograms.projections,
 *    section: section
 * }
 * info = {
 *    name: 'Physics',
 *    theme: theme,
 *    background: {
 *        url: './ndmvr/backgrounds/background1.jpg',
 *        radius: '3000',
 *        height: '2048',
 *        width: '2048'
 *     }
 *  }
 *  return (
 *  <NdmVrScene data={data} info={info} />
 * )
 */
const NdmVrScene = ({ data, info }) => {
  return (
    <a-scene
      embedded
      style={{
        zIndex: '1',
        width: '100%',
        minHeight: 'calc(90vh)'
      }}
    >
      <a-assets>
        <img id='skyTexture' src={info.background.url} alt='background' />
      </a-assets>

      <NdmVrCamera />
      <NdmVrHistogram3D
        name={info.name}
        histogram={data.histogram}
        histoSection={data.section}
        projections={data.projections}
        range={data.range}
        theme={info.theme}
      />
      <a-sky
        height={info.background.height ? info.background.height : '2048'}
        radius={info.background.radius ? info.background.radius : '3000'}
        width={info.background.width ? info.background.width : '2048'}
        src={
          info.background.url
            ? info.background.url
            : './ndmvr/backgrounds/background1.jpg'
        }
        position={`${info.background.xPosition ? info.background.xPosition : 0}
        ${info.background.yPosition ? info.background.yPosition : 1000}
        ${info.background.zPosition ? info.background.zPosition : 1000}`}
      />
    
    </a-scene>
  )
}

export default NdmVrScene
