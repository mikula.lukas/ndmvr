/* add listeners for keyboard controller */
/**
 * Modul obsahuje implementované metódy, ktoré reagujú na vstupy používateľa.
 * Funkcie sú určené pre komponenty slúžiace definovanie listenerov pre klávesnicu počítača.
 * @module keyboardController
 */
import { CameraService } from '../services/cameraService'
import {
  histogramTH2Service,
  histogramTH3Service
} from '../observables/histogramSubject'
import binSubject from '../observables/binSubject'
import jsrootService from '../services/jsrootService'
import cameraSubject from '../observables/cameraSubject'

const keyPressed = {}
let cameraService = new CameraService()
let aframeObj
let geoAttributes

/**
 * Získanie objektu pre prístup k odberu signálov a dát.
 * @param {string} property - Definuje názov atribútu histogramu (poloha, rozmer, rotácia), ktorý bude modifikovaný
 * @param {Event} event - Definuje Event obsahujúci informácie o používateľovej interakcií
 * @return {void}
 */
const handlePositioning = (property, event) => {
  let difference = 0.2

  if (property.name === 'scale') difference = 0.01
  else if (property.name === 'rotation') difference = 2

  if (event.key === 'ArrowDown') {
    property.xValue = property.xValue + difference
  } else if (event.key === 'ArrowUp') {
    property.xValue = property.xValue - difference
  } else if (event.key === '=') {
    property.yValue = property.yValue + difference
  } else if (event.key === '-') {
    property.yValue = property.yValue - difference
  } else if (event.key === 'ArrowRight') {
    property.zValue = property.zValue + difference
  } else if (event.key === 'ArrowLeft') {
    property.zValue = property.zValue - difference
  }
  aframeObj.setAttribute(
    property.name,
    `${property.xValue} ${property.yValue} ${property.zValue}`
  )
}

// handle histogram section change by default range = 1
/**
 * Zmena zobrazovanej sekcie histogramu o predvolený rozsah (1) na klávesnici
 * @param {Event} event - Definuje Event obsahujúci informácie o používateľovej interakcií
 * @return {void}
 */
const handleChangeHistogramSectionByDefaultRange = (event) => {
  if (event.key === 'k' || event.key === 'K') {
    histogramTH2Service.changeHistogramSectionByOffset(
      'yOffset',
      'TH2',
      false,
      true
    )
    histogramTH3Service.changeHistogramSectionByOffset(
      'yOffset',
      'TH3',
      false,
      true
    )
  } else if (event.key === 'i' || event.key === 'I') {
    histogramTH2Service.changeHistogramSectionByOffset(
      'yOffset',
      'TH2',
      true,
      true
    )
    histogramTH3Service.changeHistogramSectionByOffset(
      'yOffset',
      'TH3',
      true,
      true
    )
  } else if (event.key === 'u' || event.key === 'U') {
    histogramTH3Service.changeHistogramSectionByOffset(
      'zOffset',
      'TH3',
      true,
      true
    )
  } else if (event.key === 'o' || event.key === 'O') {
    histogramTH3Service.changeHistogramSectionByOffset(
      'zOffset',
      'TH3',
      false,
      true
    )
  } else if (event.key === 'l' || event.key === 'L') {
    histogramTH2Service.changeHistogramSectionByOffset(
      'xOffset',
      'TH2',
      false,
      true
    )
    histogramTH3Service.changeHistogramSectionByOffset(
      'xOffset',
      'TH3',
      false,
      true
    )
  } else if (event.key === 'j' || event.key === 'J') {
    histogramTH2Service.changeHistogramSectionByOffset(
      'xOffset',
      'TH2',
      true,
      true
    )
    histogramTH3Service.changeHistogramSectionByOffset(
      'xOffset',
      'TH3',
      true,
      true
    )
  }
}

// handle histogram section change by user's range = user sets this range
/**
 * Zmena zobrazovanej sekcie histogramu o vlastný rozsah (podľa nastavení používateľa) na klávesnici
 * @param {Event} event - Definuje Event obsahujúci informácie o používateľovej interakcií
 * @return {void}
 */
const handleChangeHistogramSectionByOwnRange = (event) => {
  if (event.key === 'k' || event.key === 'K') {
    histogramTH2Service.changeHistogramSectionByOffset(
      'yOffset',
      'TH2',
      false,
      false
    )
    histogramTH3Service.changeHistogramSectionByOffset(
      'yOffset',
      'TH3',
      false,
      false
    )
  } else if (event.key === 'i' || event.key === 'I') {
    histogramTH2Service.changeHistogramSectionByOffset(
      'yOffset',
      'TH2',
      true,
      false
    )
    histogramTH3Service.changeHistogramSectionByOffset(
      'yOffset',
      'TH3',
      true,
      false
    )
  } else if (event.key === 'u' || event.key === 'U') {
    histogramTH3Service.changeHistogramSectionByOffset(
      'zOffset',
      'TH3',
      true,
      false
    )
  } else if (event.key === 'o' || event.key === 'O') {
    histogramTH3Service.changeHistogramSectionByOffset(
      'zOffset',
      'TH3',
      false,
      false
    )
  } else if (event.key === 'l' || event.key === 'L') {
    histogramTH2Service.changeHistogramSectionByOffset(
      'xOffset',
      'TH2',
      false,
      false
    )
    histogramTH3Service.changeHistogramSectionByOffset(
      'xOffset',
      'TH3',
      false,
      false
    )
  } else if (event.key === 'j' || event.key === 'J') {
    histogramTH2Service.changeHistogramSectionByOffset(
      'xOffset',
      'TH2',
      true,
      false
    )
    histogramTH3Service.changeHistogramSectionByOffset(
      'xOffset',
      'TH3',
      true,
      false
    )
  }
}

// key press handler
/**
 * Funkcia, vukonávajúca sa na klik používateľa.
 * Funkcia detekuje, uloží stlačenú klávesu do poľa stlašených kláves a zavolá náležitú funkciu
 * @param {Event} event - Definuje Event obsahujúci informácie o používateľovej interakcií
 * @return {void}
 */
const keyPressHandlerFunction = (event) => {
  keyPressed[event.key] = true

  if (keyPressed.Z || keyPressed.z) {
    handlePositioning(geoAttributes[0], event)
  }
  if (keyPressed.X || keyPressed.x) {
    handlePositioning(geoAttributes[1], event)
  }
  if (keyPressed.C || keyPressed.c) {
    handlePositioning(geoAttributes[2], event)
  }
  if (keyPressed.Shift) {
    handleChangeHistogramSectionByOwnRange(event)
  }
  handleChangeHistogramSectionByDefaultRange(event)
  // change camera up
  if (event.key === 'q' || event.key === 'Q') {
    cameraService.verticalMoveCamera(true, 0.3)
  }
  // change camera down
  if (event.key === 'e' || event.key === 'E') {
    cameraService.verticalMoveCamera(false, 0.3)
  }
  if (event.key === '1') {
    cameraService.setPredefinedDownPosition()
  }
  if (event.key === '2') {
    cameraService.setPredefinedUpPosition()
  }
  if (event.key === '3') {
    cameraService.setPredefinedRightPosition()
  }
  if (event.key === '4') {
    cameraService.setPredefinedLeftPosition()
  }
  if (event.key === '5') {
    cameraService.setPredefinedDownPositionWithOffset()
  }
  if (event.key === '6') {
    cameraService.setPredefinedUpPositionWithOffset()
  }
  if (event.key === '7') {
    cameraService.setPredefinedRightPositionWithOffset()
  }
  if (event.key === '8') {
    cameraService.setPredefinedLeftPositionWithOffset()
  }
  if (event.key === 'Enter') {
    binSubject.saveSelectedBinToLocalStorage()
    ;[
      'th-mapping',
      'bannerId_1',
      'bannerId_2',
      'bannerId_3',
      'bannerId_4'
    ].forEach((targetId) => {
      if (document.getElementById(targetId) !== null) {
        jsrootService.displayImageOfProjection(
          'projectionContainer',
          targetId,
          '500px',
          '400px'
        )
      }
    })
  }
  if (event.key === 'r' || event.key === 'R') {
    binSubject.deleteBinFromLocalStorage()
  }
  if (event.key === 'm' || event.key === 'M') {
    histogramTH2Service.changeHistogramFunction('feet', 'TH2')
  }
  if (event.key === 'n' || event.key === 'N') {
    histogramTH2Service.changeHistogramFunction('default', 'TH2')
  }
  if (event.key === 'c' || event.key === 'C') {
    cameraSubject.setUserState()
  }
  if (event.key === 'v' || event.key === 'V') {
    cameraSubject.setVisibilityOfBanners('keyboard')
  }
  if (event.key === 'x' || event.key === 'X') {
    cameraSubject.shiftBanners()
  }
}

// delete function key from array
/**
 * Funkcia vymaže klávesu z poľa stlačených kláves
 * @param {Event} event - Definuje Event obsahujúci informácie o používateľovej interakcií
 * @return {void}
 */
const keyReleaseHandlerFunction = (event) => {
  delete keyPressed[event.key]
}

/**
 * Funkcia inicializuje všetky údaje o histograme a samotnú referenciu na histogram
 * @param {Object} data - Objekt obsahuje všetky atribúty (rozmer, pozícia, rotácia)
 * @param {HTMLElement} object - Referencia na element histogramu, ktorý sa má modifikovať
 * @return {void}
 */
const initialKeyboardController = (data, object) => {
  aframeObj = object
  geoAttributes = [data.position, data.scale, data.rotation]
  // if (this.#cameraService === null) this.#cameraService = new CameraService()
  // set initial position, scale, rotation of the histogram
  for (let i = 0; i < 3; i++) {
    aframeObj.setAttribute(
      geoAttributes[i].name,
      `${geoAttributes[i].xValue} ${geoAttributes[i].yValue} ${geoAttributes[i].zValue}`
    )
  }
}

/**
 * Funkcia aktualizuje referenciu servis kamery
 * @return {void}
 */
const keyboardUpdateCameraReference = () => {
  cameraService = new CameraService()
}

export { initialKeyboardController }
export { keyPressHandlerFunction }
export { keyReleaseHandlerFunction }
export { keyboardUpdateCameraReference }
