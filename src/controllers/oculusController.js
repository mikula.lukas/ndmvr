/* add listeners for oculus controller */
/**
 * Modul obsahuje implementované metódy, ktoré reagujú na vstupy používateľa.
 * Funkcie sú určené pre komponenty slúžiace definovanie listenerov pre ovládač Oculusu.
 * @module oculusController
 */
import { CameraService } from '../services/cameraService'
import {
  histogramTH2Service,
  histogramTH3Service
} from '../observables/histogramSubject'
import binSubject from '../observables/binSubject'
import cameraSubject from '../observables/cameraSubject'
import jsrootService from '../services/jsrootService'

/*
export class OculusController {
  #aframeObj
  #cameraService
  constructor(aframeObj) {
    this.#aframeObj = aframeObj
    this.#cameraService = new CameraService()
  }

  #thumbStick = (event) => {
    if (event.detail.y > 0.95) {
      histogramTH2Service.changeHistogramSection('yOffset', 'TH2', false)
      histogramTH3Service.changeHistogramSection('yOffset', 'TH3', false)
    }
    if (event.detail.y < -0.95) {
      histogramTH2Service.changeHistogramSection('yOffset', 'TH2', true)
      histogramTH3Service.changeHistogramSection('yOffset', 'TH3', true)
    }
    if (event.detail.x < -0.95) {
      histogramTH2Service.changeHistogramSection('xOffset', 'TH2', false)
      histogramTH3Service.changeHistogramSection('xOffset', 'TH3', false)
    }
    if (event.detail.x > 0.95) {
      histogramTH2Service.changeHistogramSection('xOffset', 'TH2', true)
      histogramTH3Service.changeHistogramSection('xOffset', 'TH3', true)
    }
  }

  // set listeners for positioning
  setControllerOnEntity(data) {
    const aframeObj = this.#aframeObj
    const geoAttributes = [data.position, data.scale, data.rotation]
    // if (this.#cameraService === null) this.#cameraService = new CameraService()
    // set initial position, scale, rotation of the histogram
    for (let i = 0; i < 3; i++) {
      this.#aframeObj.setAttribute(
        geoAttributes[i].name,
        `${geoAttributes[i].xValue} ${geoAttributes[i].yValue} ${geoAttributes[i].zValue}`
      )
    }

    // add positioning listeners
    aframeObj.addEventListener('thumbstickmoved', this.#thumbStick)
    aframeObj.addEventListener('xbuttondown', () => {
      this.#cameraService.verticalMoveCamera(false)
    })
    aframeObj.addEventListener('ybuttondown', () => {
      this.#cameraService.verticalMoveCamera(true)
    })
  }
}
*/

// let aframeObj
let cameraService = new CameraService()

/**
 * Zmena zobrazovanej sekcie histogramu o predvolený rozsah (1) na ovladači Oculusu.
 * @param {Event} event - Definuje Event obsahujúci informácie o používateľovej interakcií
 * @return {void}
 */
const thumbStickByOwnOffset = (event) => {
  if (event.detail.y > 0.95) {
    histogramTH2Service.changeHistogramSectionByOffset(
      'yOffset',
      'TH2',
      false,
      false
    )
    histogramTH3Service.changeHistogramSectionByOffset(
      'yOffset',
      'TH3',
      false,
      false
    )
  }
  if (event.detail.y < -0.95) {
    histogramTH2Service.changeHistogramSectionByOffset(
      'yOffset',
      'TH2',
      true,
      false
    )
    histogramTH3Service.changeHistogramSectionByOffset(
      'yOffset',
      'TH3',
      true,
      false
    )
  }
  if (event.detail.x < -0.95) {
    histogramTH2Service.changeHistogramSectionByOffset(
      'xOffset',
      'TH2',
      true,
      false
    )
    histogramTH3Service.changeHistogramSectionByOffset(
      'xOffset',
      'TH3',
      true,
      false
    )
  }
  if (event.detail.x > 0.95) {
    histogramTH2Service.changeHistogramSectionByOffset(
      'xOffset',
      'TH2',
      false,
      false
    )
    histogramTH3Service.changeHistogramSectionByOffset(
      'xOffset',
      'TH3',
      false,
      false
    )
  }
}

/**
 * Zmena zobrazovanej sekcie histogramu o špecifikovaný rozsah (podľa nastavení používateľa) na ovladači Oculusu.
 * @param {Event} event - Definuje Event obsahujúci informácie o používateľovej interakcií
 * @return {void}
 */
const thumbStickByDefaultOffset = (event) => {
  if (event.detail.y > 0.95) {
    histogramTH2Service.changeHistogramSectionByOffset(
      'yOffset',
      'TH2',
      false,
      true
    )
    histogramTH3Service.changeHistogramSectionByOffset(
      'yOffset',
      'TH3',
      false,
      true
    )
  }
  if (event.detail.y < -0.95) {
    histogramTH2Service.changeHistogramSectionByOffset(
      'yOffset',
      'TH2',
      true,
      true
    )
    histogramTH3Service.changeHistogramSectionByOffset(
      'yOffset',
      'TH3',
      true,
      true
    )
  }
  if (event.detail.x < -0.95) {
    histogramTH2Service.changeHistogramSectionByOffset(
      'xOffset',
      'TH2',
      true,
      true
    )
    histogramTH3Service.changeHistogramSectionByOffset(
      'xOffset',
      'TH3',
      true,
      true
    )
  }
  if (event.detail.x > 0.95) {
    histogramTH2Service.changeHistogramSectionByOffset(
      'xOffset',
      'TH2',
      false,
      true
    )
    histogramTH3Service.changeHistogramSectionByOffset(
      'xOffset',
      'TH3',
      false,
      true
    )
  }
}

/**
 * Funkcia, zabezpečujúca horizontálny pohyb kamery po osiach.
 * @param {Event} event - Definuje Event obsahujúci informácie o používateľovej interakcií
 * @param {number} speed - Definuje rýchlosť pohybu
 * @return {void}
 */
const thumbStickForMoving = (event, speed) => {
  if (event.detail.y > 0.95) {
    cameraService.horizontalMoveCamera('z', false, speed)
  }
  if (event.detail.y < -0.95) {
    cameraService.horizontalMoveCamera('z', true, speed)
  }
  if (event.detail.x < -0.95) {
    cameraService.horizontalMoveCamera('x', true, speed)
  }
  if (event.detail.x > 0.95) {
    cameraService.horizontalMoveCamera('x', false, speed)
  }
}

/**
 * Funkcia, zabezpečujúca zmenu pozície kamery na vopred definovanú pozíciu (len X a Z súradnica).
 * @param {Event} event - Definuje Event obsahujúci informácie o používateľovej interakcií
 * @return {void}
 */
const thumbStickPredefinedPosition = (event) => {
  if (event.detail.y > 0.95) {
    cameraService.setPredefinedUpPosition()
  }
  if (event.detail.y < -0.95) {
    cameraService.setPredefinedDownPosition()
  }
  if (event.detail.x < -0.95) {
    cameraService.setPredefinedLeftPosition()
  }
  if (event.detail.x > 0.95) {
    cameraService.setPredefinedRightPosition()
  }
}

/**
 * Funkcia, zabezpečujúca zmenu pozície kamery na vopred definovanú pozíciu (X a Y a Z súradnica).
 * @param {Event} event - Definuje Event obsahujúci informácie o používateľovej interakcií
 * @return {void}
 */
const thumbStickPredefinedPositionWithOffset = (event) => {
  if (event.detail.y > 0.95) {
    cameraService.setPredefinedUpPositionWithOffset()
  }
  if (event.detail.y < -0.95) {
    cameraService.setPredefinedDownPositionWithOffset()
  }
  if (event.detail.x < -0.95) {
    cameraService.setPredefinedLeftPositionWithOffset()
  }
  if (event.detail.x > 0.95) {
    cameraService.setPredefinedRightPositionWithOffset()
  }
}

/**
 * Funkcia, zobrazenie novej sekcie smerom hore.
 * @param {boolean} defaultRange - Ak je True tak bude použitý range (1), ak je False tak (nastavený rozsah)
 * @return {void}
 */
const oculusUpSection = (defaultRange) => {
  histogramTH3Service.changeHistogramSectionByOffset(
    'zOffset',
    'TH3',
    true,
    defaultRange
  )
}

/**
 * Funkcia, zobrazenie novej sekcie smerom dolu
 * @param {boolean} defaultRange - Ak je True tak bude použitý range (1), ak je False tak (nastavený rozsah)
 * @return {void}
 */
const oculusDownSection = (typeOfRange) => {
  histogramTH3Service.changeHistogramSectionByOffset(
    'zOffset',
    'TH3',
    false,
    typeOfRange
  )
}

// const initialOculusController = (el) => {
//  aframeObj = el
// }

/**
 * Funkcia, aktualizuje servis kamery.
 * @return {void}
 */
const oculusUpdateCameraReference = () => {
  cameraService = new CameraService()
}

/**
 * Zmena zobrazovanej sekcie histogramu o špecifikovaný rozsah (podľa nastavení používateľa) na ovladači Oculusu.
 * @param {Event} event - Definuje Event obsahujúci informácie o používateľovej interakcií
 * @return {void}
 */
const oculusThumbStickFunction = (event) => {
  thumbStickByOwnOffset(event)
}

/**
 * Zmena zobrazovanej sekcie histogramu o predvolený rozsah (1) na ovladači Oculusu.
 * @param {Event} event - Definuje Event obsahujúci informácie o používateľovej interakcií
 * @return {void}
 */
const oculusThumbStickWithGripFunction = (event) => {
  thumbStickByDefaultOffset(event)
}

/**
 * Funkcia, zabezpečujúca zmenu pozície kamery na vopred definovanú pozíciu (len X a Z súradnica).
 * @param {Event} event - Definuje Event obsahujúci informácie o používateľovej interakcií
 * @return {void}
 */
const oculusThumbStickPredefinedCameraPosition = (event) => {
  thumbStickPredefinedPosition(event)
}

/**
 * Funkcia, zabezpečujúca zmenu pozície kamery na vopred definovanú pozíciu (X a Y a Z súradnica).
 * @param {Event} event - Definuje Event obsahujúci informácie o používateľovej interakcií
 * @return {void}
 */
const oculusThumbStickPredefinedCameraPositionWithOffset = (event) => {
  thumbStickPredefinedPositionWithOffset(event)
}

/**
 * Funkcia, zabezpečujúca horizontálny pohyb kamery po osiach.
 * @param {Event} event - Definuje Event obsahujúci informácie o používateľovej interakcií
 * @param {Number} speed - Definuje rýchlosť pohybu
 * @return {void}
 */
const oculusThumbStickForMoving = (event, speed) => {
  thumbStickForMoving(event, speed)
}

/**
 * Funkcia, zabezpečí posunutie kamery smerom hore
 * @param {Number} speed - Určuje rýchlosť pohybu kamery
 * @return {void}
 */
const oculusXButtonDownFunction = (speed) => {
  cameraService.verticalMoveCamera(true, speed / 2)
}

/**
 * Funkcia, zabebpečí posunutie kamery smerom dolu
 * @param {Number} speed - Určuje rýchlosť pohybu kamery
 * @return {void}
 */
const oculusYButtonDownFunction = (speed) => {
  cameraService.verticalMoveCamera(false, speed / 2)
}

/**
 * Funkcia, zabezpečí označenie binu
 * @return {void}
 */
const oculusThumbStickMarkBin = () => {
  binSubject.saveSelectedBinToLocalStorage()
}

/**
 * Funkcia, zabezpečí odznačenie binu
 * @return {void}
 */
const oculusThumbStickUnmarkBin = () => {
  binSubject.deleteBinFromLocalStorage()
}

/**
 * Funkcia, zabezpečí prepnutie do špeciálneho módu, určeného funkciou
 * @return {void}
 */
const oculusShowFunctionView = () => {
  histogramTH2Service.changeHistogramFunction('feet', 'TH2')
}

/**
 * Funkcia, zabezpečí prepnutie do predvoleného vizualizačného módu
 * @return {void}
 */
const oculusShowDefaultView = () => {
  histogramTH2Service.changeHistogramFunction('default', 'TH2')
}

/**
 * Funkcia, zabezpečí prepínanie medzi zobrazeniami
 * @return {void}
 */
const oculusSwitchViewWithBanners = () => {
  cameraSubject.setVisibilityOfBanners('oculus')
}

/**
 * Funkcia, zabezpečí zmenu obsahu na paneloch
 * @return {void}
 */
const oculusChangeBannerContent = () => {
  cameraSubject.setUserState()
}

/**
 * Funkcia, zabezpečí rotovanie panelov
 * @return {void}
 */
const oculusShiftBanners = () => {
  cameraSubject.shiftBanners()
}

/**
 * Funkcia, prekreslí náhľady na paneloch vo VR
 * @return {void}
 */
const oculusRedrawHistogramBanners = () => {
  ;[
    'th-mapping',
    'bannerId_1',
    'bannerId_2',
    'bannerId_3',
    'bannerId_4'
  ].forEach((targetId) => {
    if (document.getElementById(targetId) !== null) {
      jsrootService.displayImageOfProjection(
        'projectionContainer',
        targetId,
        '500px',
        '400px'
      )
    }
  })
}

// export { initialOculusController }
export { oculusRedrawHistogramBanners }
export { oculusShiftBanners }
export { oculusChangeBannerContent }
export { oculusSwitchViewWithBanners }
export { oculusShowDefaultView }
export { oculusShowFunctionView }
export { oculusThumbStickUnmarkBin }
export { oculusThumbStickMarkBin }
export { oculusThumbStickFunction }
export { oculusThumbStickWithGripFunction }
export { oculusXButtonDownFunction }
export { oculusYButtonDownFunction }
export { oculusUpdateCameraReference }
export { oculusThumbStickPredefinedCameraPosition }
export { oculusThumbStickPredefinedCameraPositionWithOffset }
export { oculusUpSection }
export { oculusDownSection }
export { oculusThumbStickForMoving }
