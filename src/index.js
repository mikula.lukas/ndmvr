// imports
import JSrootHistogram from './components/JsrootHistogram'
import NdmVrScene from './components/NdmVrHistogramScene'
import HistogramTH2 from './components/AframeComponent'
import { NdmVrStorageService } from './services/ndmVrStorageService'
import Banner from './aframeComponents/infoBannerAframeComponent'
import LeftController from './aframeComponents/leftOculusController'
import RightController from './aframeComponents/rightOculusController'
import binDataDistributor from './observables/binDataDistributor'

// create services
const ndmVrStorage = new NdmVrStorageService()

// export
export {
  // react components
  JSrootHistogram,
  NdmVrScene,
  // export aframe components
  Banner,
  LeftController,
  RightController,
  HistogramTH2,
  // export necessary services
  ndmVrStorage,
  // rxjs bin data distributor
  binDataDistributor
}
