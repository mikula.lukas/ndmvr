// bin distributor
/** @module BinDataDistributor */
import { Subject } from 'rxjs'

/**
 * Subjekt pre publikáciu signálov a dát pre biny.
 * @class
 */
class BinDataDistributor {
  #subject
  constructor() {
    this.#subject = new Subject()
  }

  /**
   * Publikácia dat oznaceneho binu.
   * @return {void}
   */
  sendDataOfTheSelectedBin(binData) {
    this.#subject.next(binData)
  }

  /**
   * Získanie objektu pre prístup k odberu signálov a dát.
   * @return {Subject} - Vracia inštanciu objektu, na ktorý sa dá prihlásiť a odoberať publikované informácie a signály
   */
  getBinDistributor() {
    return this.#subject.asObservable()
  }
}

const binDataDistributor = new BinDataDistributor()
export default binDataDistributor
