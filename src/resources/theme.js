export default {
  def: {
    colorPalette: [
      '#4b0066',
      '#8300b3',
      '#bb00ff',
      '#cf4dff',
      '#e499ff',
      '#ffffcc',
      '#ffff99',
      '#ffff66',
      '#ffff33',
      '#fcba03'
    ],
    axis: {
      x: '#ad0c00',
      y: '#0206de',
      z: '#007d1d'
    },
    accent: {
      primary: '#4b0066',
      secondary: '#FF3700'
    },
    font: {
      // primary text titles
      primary: 'black',
      // secondary labels and atc
      secondary: 'black'
    }
  },
  fire: {
    colorPalette: [
      '#5b0000',
      '#7b2100',
      '#a23b00',
      '#c84900',
      '#df5900',
      '#f18800',
      '#f1a000',
      '#ffae00',
      '#ffcd19',
      '#ffe159'
    ],
    axis: {
      x: '#ad0c00',
      y: '#0206de',
      z: '#007d1d'
    },
    accent: {
      primary: '#5b0000',
      secondary: '#b000d4'
    },
    font: {
      primary: 'black',
      secondary: 'black'
    }
  },
  nebula: {
    colorPalette: [
      '#13004b',
      '#320065',
      '#4e0085',
      '#6100a5',
      '#7000b8',
      '#9800c6',
      '#ab00c6',
      '#b900d2',
      '#d700da',
      '#ff6efc'
    ],
    axis: {
      x: '#ad0c00',
      y: '#0206de',
      z: '#007d1d'
    },
    accent: {
      primary: '#13004b',
      secondary: '#ff3700'
    },
    font: {
      // primary text titles
      primary: 'black',
      // secondary labels and atc
      secondary: 'black'
    }
  },
  rainforest: {
    colorPalette: [
      '#005b01',
      '#007b23',
      '#00a23d',
      '#00c84c',
      '#00df5c',
      '#00f18b',
      '#00ffb1',
      '#19ffd0',
      '#a6fffe',
      '#56fffe'
    ],
    axis: {
      x: '#ad0c00',
      y: '#0206de',
      z: '#007d1d'
    },
    accent: {
      primary: '#005b01',
      secondary: '#ff3700'
    },
    font: {
      // primary text titles
      primary: 'black',
      // secondary labels and atc
      secondary: 'black'
    }
  },
  nature: {
    colorPalette: [
      '#4a691a',
      '#5e8425',
      '#79a834',
      '#91c640',
      '#9ed64b',
      '#a8dc59',
      '#b0e166',
      '#bfeb7c',
      '#d7fba2',
      '#deffad'
    ],
    axis: {
      x: '#ad0c00',
      y: '#0206de',
      z: '#007d1d'
    },
    accent: {
      primary: '#4a691a',
      secondary: '#b000d4'
    },
    font: {
      // primary text titles
      primary: 'black',
      // secondary labels and atc
      secondary: 'black'
    }
  },
  water: {
    colorPalette: [
      '#00545a',
      '#00717a',
      '#0097a2',
      '#00bac8',
      '#00cede',
      '#00dff0',
      '#00edff',
      '#45f2ff',
      '#7ef6ff',
      '#bdfaff'
    ],
    axis: {
      x: '#ad0c00',
      y: '#0206de',
      z: '#007d1d'
    },
    accent: {
      primary: '#00545a',
      secondary: '#ff3700'
    },
    font: {
      // primary text titles
      primary: 'black',
      // secondary labels and atc
      secondary: 'black'
    }
  },
  sunflower: {
    colorPalette: [
      '#1e3600',
      '#2e4d00',
      '#507001',
      '#91a004',
      '#b4bb06',
      '#d2d309',
      '#eeed0f',
      '#ffff28',
      '#ffff85',
      '#ffffc2'
    ],
    axis: {
      x: '#ad0c00',
      y: '#0206de',
      z: '#007d1d'
    },
    accent: {
      primary: '#1e3600',
      secondary: '#ff3700'
    },
    font: {
      // primary text titles
      primary: 'black',
      // secondary labels and atc
      secondary: 'black'
    }
  },
  blood: {
    colorPalette: [
      '#470101',
      '#6e0101',
      '#800b0b',
      '#940c0c',
      '#a30000',
      '#bd0909',
      '#e01f1f',
      '#d43b3b',
      '#db5e5e',
      '#f78888'
    ],
    axis: {
      x: '#ad0c00',
      y: '#0206de',
      z: '#007d1d'
    },
    accent: {
      primary: '#470101',
      secondary: '#f6ff00'
    },
    font: {
      // primary text titles
      primary: 'black',
      // secondary labels and atc
      secondary: 'black'
    }
  },
  magma: {
    colorPalette: [
      '#dbff1d',
      '#d5e814',
      '#d5d007',
      '#cc9b00',
      '#b87600',
      '#9c4a00',
      '#812f00',
      '#661900',
      '#661420',
      '#2b0000'
    ],
    axis: {
      x: '#8a0000',
      y: '#03017a',
      z: '#015e03'
    },
    accent: {
      primary: '#2b0000',
      secondary: '#ae2000'
    },
    font: {
      // primary text titles
      primary: 'black',
      // secondary labels and atc
      secondary: 'black'
    }
  },
  jupiter: {
    colorPalette: [
      '#00340f',
      '#00344f',
      '#006e3a',
      '#00976a',
      '#26efd0',
      '#9c4a00',
      '#9c4a00',
      '#812f00',
      '#661900',
      '#2b0000'
    ],
    axis: {
      x: '#8a0000',
      y: '#03017a',
      z: '#015e03'
    },
    accent: {
      primary: '#00340f',
      secondary: '#e3ff5b'
    },
    font: {
      // primary text titles
      primary: 'black',
      // secondary labels and atc
      secondary: 'black'
    }
  },
  magic: {
    colorPalette: [
      '#342000',
      '#6e5e00',
      '#888500',
      '#919700',
      '#8dbe00',
      '#caef26',
      '#cf6e85',
      '#b300b8',
      '#6b0090',
      '#3b0066'
    ],
    axis: {
      x: '#8a0000',
      y: '#03017a',
      z: '#015e03'
    },
    accent: {
      primary: '#342000',
      secondary: '#c85bff'
    },
    font: {
      // primary text titles
      primary: 'black',
      // secondary labels and atc
      secondary: 'black'
    }
  }
}
