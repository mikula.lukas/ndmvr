// service to manage camera
/** @module CameraService */

/**
 * Servis pre ovládanie a manipuláciu pozície kamery.
 * @class
 */
export class CameraService {
  #cameraWrapper
  constructor() {
    this.#cameraWrapper = document.getElementById('cameraWrapper')
  }

  /**
   * Zmena vertikálnej polohy kamery.
   * @param {boolean} moveUp - Ak je True, kamera vyššie ak je False kamera nižšie
   * @param {number} speed - Rýchlosť zmeny pozície kamery
   * @return {void}
   */
  verticalMoveCamera = (moveUp, speed) => {
    // if we have not a cameraWrapper
    if (this.#cameraWrapper === null)
      this.#cameraWrapper = document.getElementById('cameraWrapper')
    if (this.#cameraWrapper !== null) {
      if (moveUp) {
        this.#cameraWrapper.object3D.position.y += speed
      } else {
        if (this.#cameraWrapper.object3D.position.y > 0)
          this.#cameraWrapper.object3D.position.y -= speed
      }
    }
  }

  /**
   * Zmena horizontálnej polohy kamery.
   * @param {string} axis - Os po ktorej je ralizovaný pohyb kamery
   * @param {number} increment - Hodnota posunu kamery
   * @param {number} speed - Rýchlosť zmeny pozície kamery
   * @return {void}
   */
  horizontalMoveCamera = (axis, increment, speed) => {
    // if we have not a cameraWrapper
    if (this.#cameraWrapper === null)
      this.#cameraWrapper = document.getElementById('cameraWrapper')
    if (this.#cameraWrapper !== null) {
      if (increment) {
        this.#cameraWrapper.object3D.position[axis] += speed
      } else {
        this.#cameraWrapper.object3D.position[axis] -= speed
      }
    }
  }

  /**
   * Zmena vertikálnej polohy kamery.
   * @param {HTMLElement} elm - Cieľový element
   * @param {Object} offset - Ofsety zobrazenej sekcie
   * @param {number} rotation - Rotácia kamery
   * @return {void}
   */
  #setVerticalOffset = (elm, offset, rotation) => {
    if (this.#cameraWrapper !== null && elm !== null) {
      // this.#cameraWrapper.object3D.rotation.y = rotation
      const newXPos = elm.object3D.position.x + 1 * elm.object3D.position.x
      const newZPos = elm.object3D.position.z + 1 * elm.object3D.position.z
      const newYPos = offset
      // this.#cameraWrapper.object3D.position.x = newXPos + 1
      // this.#cameraWrapper.object3D.position.z = newZPos + 1
      // this.#cameraWrapper.object3D.position.y = newYPos
      this.#cameraWrapper.setAttribute(
        'animation',
        `property: position; to: ${newXPos + 1} ${newYPos} ${
          newZPos + 1
        }; dur: 100;`
      )
      this.#cameraWrapper.setAttribute(
        'animation__1',
        `property: rotation; to: ${
          this.#cameraWrapper.object3D.rotation.x
        } ${rotation} ${this.#cameraWrapper.object3D.rotation.z}; dur: 100;`
      )
    }
  }

  /**
   * Zmena vertikálnej polohy kamery podľa ofsetov.
   * @param {Object} offsets - Ofsety zobrazenej sekcie
   * @return {void}
   */
  setCameraPosition = (offsets) => {
    if (this.#cameraWrapper !== null) {
      let newZPos = this.#cameraWrapper.object3D.position.y
      if (offsets.zOffset !== undefined) {
        // this.#cameraWrapper.object3D.position.y = offsets.zOffset + 2
        newZPos = offsets.zOffset * 2
      }
      const newXPos = offsets.xOffset + offsets.xOffset
      const newYPos = offsets.yOffset + offsets.yOffset
      // this.#cameraWrapper.object3D.position.x = newXPos - 3
      // this.#cameraWrapper.object3D.position.z = newYPos - 3
      setTimeout(() => {
        this.#cameraWrapper.setAttribute(
          'animation',
          `property: position; to: ${newXPos - 3} ${newZPos} ${
            newYPos - 3
          }; dur: 40;`
        )
      }, 1800)
    }
  }

  /**
   * Zmena polohy kamery podľa predefinovanej pozície.
   * @return {void}
   */
  setPredefinedDownPosition = () => {
    // get target element
    const labelElement = document.getElementById('downLabel')
    if (labelElement !== undefined) {
      this.#setVerticalOffset(labelElement, 0, 180)
    }
  }

  /**
   * Zmena polohy kamery podľa predefinovanej pozície
   * @return {void}
   */
  setPredefinedUpPosition = () => {
    // get target element
    const labelElement = document.getElementById('upLabel')
    if (labelElement !== undefined) {
      this.#setVerticalOffset(labelElement, 0, 0)
    }
  }

  /**
   * Zmena polohy kamery podľa predefinovanej pozície
   * @return {void}
   */
  setPredefinedRightPosition = () => {
    // get target element
    const labelElement = document.getElementById('rightLabel')
    if (labelElement !== undefined) {
      this.#setVerticalOffset(labelElement, 0, 90)
    }
  }

  /**
   * Zmena polohy kamery podľa predefinovanej pozície
   * @return {void}
   */
  setPredefinedLeftPosition = () => {
    // get target element
    const labelElement = document.getElementById('leftLabel')
    if (labelElement !== undefined) {
      this.#setVerticalOffset(labelElement, 0, 270)
    }
  }

  /**
   * Zmena polohy kamery podľa predefinovanej pozície
   * @return {void}
   */
  setPredefinedDownPositionWithOffset = () => {
    // get target element
    const labelElement = document.getElementById('downLabel')
    if (labelElement !== undefined) {
      this.#setVerticalOffset(labelElement, 10, 180)
    }
  }

  /**
   * Zmena polohy kamery podľa predefinovanej pozície
   * @return {void}
   */
  setPredefinedUpPositionWithOffset = () => {
    // get target element
    const labelElement = document.getElementById('upLabel')
    if (labelElement !== undefined) {
      this.#setVerticalOffset(labelElement, 10, 0)
    }
  }

  /**
   * Zmena polohy kamery podľa predefinovanej pozície
   * @return {void}
   */
  setPredefinedRightPositionWithOffset = () => {
    // get target element
    const labelElement = document.getElementById('rightLabel')
    if (labelElement !== undefined) {
      this.#setVerticalOffset(labelElement, 10, 90)
    }
  }

  /**
   * Zmena polohy kamery podľa predefinovanej pozície
   * @return {void}
   */
  setPredefinedLeftPositionWithOffset = () => {
    // get target element
    const labelElement = document.getElementById('leftLabel')
    if (labelElement !== undefined) {
      this.#setVerticalOffset(labelElement, 10, 270)
    }
  }
}
