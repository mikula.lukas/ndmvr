// service for creating histogram projections
/** @module JsrootService */

// eslint-disable-next-line no-unused-vars
import { createDemoTH2Histogram } from '../../example/src/utilsScripts/utilsPageScript'

/**
 * Servis pre vytváranie JSROOT projekcií.
 * @class
 */
class JsrootService {
  #jsroot = null

  /**
   * Funkcia zabezpečí nastavenie objektu JSROOT servisu.
   * @param {Object} JSROOT - Object JSROOT
   * @return {void}
   */
  set jsrootLibrary(JSROOT) {
    this.#jsroot = JSROOT
  }

  get jsrootLibrary() {
    return this.#jsroot
  }

  // create projection depends on selected axis
  /**
   * Funkcia pre vytvorenie TH1 projekcie.
   * @param {string} projectionAxis - Hlaná os podľa, ktorej sa vytvorí projekcia
   * @param {Object} info - Objekt obsahujúci potrebné objekt histogramu a objekt obsahujúci informácie o zvolenom bine
   * @param {string} idTargetElm - Identifikátor cieľovej entity, do ktorej sa vykreslí výsledná projekcia
   * @param {Array} axisArray - Pole osí, obsahuje 2 osi na základe ktorých sa projekcia vytvorí
   * @return {void}
   */
  createTH1Projection(projectionAxis, info, idTargetElm, axisArray) {
    if (!info || !projectionAxis || this.#jsroot === null) {
      return
    }

    const projectionTargetAxis = projectionAxis === axisArray[0]
    const primary = projectionTargetAxis ? axisArray[0] : axisArray[1]
    const secondary = projectionTargetAxis ? axisArray[1] : axisArray[0]

    const primaryAxis = `f${secondary}axis`
    const secondaryAxis = `f${primary}axis`
    const binAxis = `${primary.toLowerCase()}Pos`

    // console.log(secondaryAxis)
    // console.log(binAxis)
    // projection X

    const h1 = this.#jsroot.createHistogram(
      'TH1I',
      info.obj[primaryAxis].fNbins
    )

    // fill important histogram info
    h1.fXaxis.fXmin = info.obj[primaryAxis].fXmin
    h1.fXaxis.fXmax = info.obj[primaryAxis].fXmax
    h1.fXaxis.fTitle = info.obj[primaryAxis].fTitle
    h1.fXaxis.fLabels = info.obj[primaryAxis].fLabels

    const currLabel =
      info.obj[secondaryAxis].fLabels?.arr.length > 0
        ? info.obj[secondaryAxis].fLabels.arr[info.bin[binAxis] - 1].fString
        : ''

    for (let n = 0; n < info.obj[primaryAxis].fNbins; n++) {
      console.log(info.obj)
      h1.setBinContent(
        n + 1,
        projectionAxis === axisArray[0]
          ? info.obj.getBinContent(info.bin[binAxis], n + 1)
          : info.obj.getBinContent(n + 1, info.bin[binAxis])
      )
    }
    h1.fName = 'px'
    h1.fTitle =
      `Projection of bin=${projectionAxis}` +
      (info.bin.binName !== '' ? info.bin.name : info.bin[binAxis] - 1) +
      `[` +
      currLabel +
      `]`
    this.#jsroot.redraw(idTargetElm, h1)
  }

  /**
   * Funkcia pre prečítanie objektu projekcie z adresárovej štruktúry získanej z root súboru.
   * @param {string} projectionAxis - Hlaná os podľa, ktorej sa vytvorí projekcia
   * @param {Object} info - Objekt obsahujúci potrebné objekt histogramu a objekt obsahujúci informácie o zvolenom bine
   * @param {string} idTargetElm - Identifikátor cieľovej entity, do ktorej sa vykreslí výsledná projekcia
   * @param {Array} axisArray - Pole osí, obsahuje 2 osi na základe ktorých sa projekcia vytvorí
   * @param {Object} projections - Adresárová štruktúra obsahujúca projekcie
   * @param {number} projIndex - Index projekcie, ktorá sa má vykresliť
   * @return {void}
   */
  openTH1Projection(
    projectionAxis,
    info,
    idTargetElm,
    axisArray,
    projections,
    projIndex
  ) {
    // const xLength = info.obj.fXaxis.fXmax / info.obj.fXaxis.fNbins
    // const yLength = info.obj.fYaxis.fXmax / info.obj.fYaxis.fNbins
    // const xBin = info.bin.xMin / xLength
    // const yBin = info.bin.yMin / yLength
    const xBin = info.bin.xPos - 1
    const yBin = info.bin.yPos - 1
    try {
      const projection =
        projections.fFolders.arr[xBin].fFolders.arr[yBin].fFolders.arr[
          projIndex || 0
        ]
      this.#jsroot.redraw(idTargetElm, projection)
      setTimeout(() => {
        this.displayImageOfProjection(
          'projectionContainer',
          'th-mapping',
          '500px',
          '400px'
        )
      }, 200)
    } catch (e) {
      console.log("you don't have this projection")
    }
  }

  /**
   * Funkcia získa aktuálny náhľad projekcie SVG elementu a zobrazí ju na panely vo VR
   * @param {string} idSourceElement - Identifikátor zdrojového elementu obsahujúceho SVG projekciu
   * @param {string} idTargetElement - Identifikátor cieľového elementu vo VR, kde bude vložený náhľad projekcie
   * @param {number} width - Šírka pre získanie náhľadu projekcie
   * @param {number} height - Výška pre získanie náhľadu projekcie
   * @return {void}
   */
  displayImageOfProjection(idSourceElement, idTargetElement, width, height) {
    try {
      const sourceSvgElement =
        document.getElementById(idSourceElement).firstElementChild
      const targetElement = document.getElementById(idTargetElement)
      const canvas = document.createElement('canvas')
      // set width an height of elements
      sourceSvgElement.setAttribute('height', height)
      sourceSvgElement.setAttribute('width', width)
      canvas.setAttribute('height', height)
      canvas.setAttribute('width', width)

      // eslint-disable-next-line no-undef
      const svgString = new XMLSerializer().serializeToString(sourceSvgElement)
      const ctx = canvas.getContext('2d')
      // eslint-disable-next-line no-undef
      const DOMURL = self.URL || self.webkitURL || self
      // eslint-disable-next-line no-undef
      const img = new Image()
      // create object url of canvas using blob object
      // eslint-disable-next-line no-undef
      const svg = new Blob([svgString], { type: 'image/svg+xml;charset=utf-8' })
      const url = DOMURL.createObjectURL(svg)

      // the function is executed after load url of the image
      img.onload = function () {
        // draw png image and create url
        ctx.drawImage(img, 0, 0)
        const png = canvas.toDataURL('image/png')
        // targetElement.setAttribute('src', png)
        targetElement.setAttribute('material', `src`, png)
        DOMURL.revokeObjectURL(png)
      }
      img.src = url
    } catch (e) {}
  }
}

const jsrootService = new JsrootService()
export default jsrootService
