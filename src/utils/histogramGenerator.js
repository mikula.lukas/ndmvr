/** @module HistogramReactFactory */
import { localStorageService } from '../services/ndmVrStorageService'
import 'aframe'
import ThemeProvider from '../resources/themeProvider'

/**
 * Vytvorenie objektu pre generovanie entít histogramu.
 * @class HistogramReactFactory - Histogram generator
 */
export default class HistogramReactFactory {
  #id = ''
  #selectedBins = []
  #histogram = null
  #type = ''
  #projections = null
  #themeProvider = null
  // current offsets
  #xOffset = 1
  #yOffset = 1
  #zOffset = 1
  // current histogram limits
  #xLimit = 0
  #yLimit = 0
  #zLimit = 0
  // bin scales
  #xWidth = 1
  #yWidth = 1
  #zWidth = 1
  // range
  #range = 8
  // shiftin scene (shift to initial position)
  #labels = {
    xInitial: 0,
    yInitial: 0,
    zInitial: 0
  }

  // constants
  #contentScale = 0.5
  #minDisplayedContent = 0
  #binScaleCoef = 2

  // option for animation
  #shift = {
    xOffset: 0,
    yOffset: 0,
    zOffset: 0
  }

  // older elements
  #previousBins = []

  /**
   * Inicializácia objektu pre vytváranie elementov histogramu.
   * @param {string} uniqueName - Identifikátor histogramu
   * @param {Object} histogram - Objekt histogramu určený na vizualizáciu
   * @param {Object} section - Objekt definujúci ofsety a rozsah histogramu
   * @param {number} range - Rozsah, ak sa nedefinujú ofsety ale len rozsah
   * @param {Object} projections - Objekt definujúci súborovú štruktúru s projekciami
   * @param {string} theme - string Definujúci názov témy, ktorá má byť použitá pri vizualizovaní
   * @return {void}
   */
  constructor(uniqueName, histogram, section, range, projections, theme) {
    this.initializeFactory(
      uniqueName,
      histogram,
      section,
      range,
      projections,
      theme
    )
  }

  /**
   * Inicializácia objektu pre vytváranie elementov histogramu.
   * @param {string} uniqueName - Identifikátor histogramu
   * @param {Object} histogram - Objekt histogramu určený na vizualizáciu
   * @param {Object} section - Objekt definujúci ofsety a rozsah histogramu
   * @param {number} range - Rozsah, ak sa nedefinujú ofsety ale len rozsah
   * @param {Object} projections - Objekt definujúci súborovú štruktúru s projekciami
   * @param {string} theme - String definujúci názov témy, ktorá má byť použitá pri vizualizovaní
   * @return {void}
   */
  initializeFactory(uniqueName, histogram, section, range, projections, theme) {
    this.setUniqueName(uniqueName)
    this.setTheme(theme)
    this.setHistogram(histogram, projections)
    this.setSection(section)
    this.setRange(range)
  }

  /**
   * Nastaví nove unikátne meno pre histogram
   */
  setUniqueName(uniqueName) {
    this.#id = uniqueName
  }

  /**
   * Nastaví ofsety pre zobrazenie sekcie histogramu
   */
  setSection(section) {
    // set current section offsets
    if (section !== null && section !== undefined) {
      if (section.xOffset)
        this.#xOffset = this.#checkOffsetValue(section.xOffset, this.#xLimit)
      if (section.yOffset)
        this.#yOffset = this.#checkOffsetValue(section.yOffset, this.#yLimit)
    }
    // define third dimension for th3 histogram
    if (this.#histogram._typename.includes('TH3')) {
      // set current section offsets
      if (section !== null && section !== undefined) {
        if (section.zOffset)
          this.#zOffset = this.#checkOffsetValue(section.zOffset, this.#zLimit)
      }
    }
  }

  /**
   * Nastaví histogram a všetky potrebné atribúty
   */
  setHistogram(histogram, projections) {
    this.#projections = projections
    this.#histogram = histogram
    this.#type = histogram._typename
    // set current limits
    this.#xLimit = histogram.fXaxis.fNbins
    this.#yLimit = histogram.fYaxis.fNbins
    // bin scales
    this.#xWidth =
      (histogram.fXaxis.fXmax - histogram.fXaxis.fXmin) /
      histogram.fXaxis.fNbins
    this.#yWidth =
      (histogram.fYaxis.fXmax - histogram.fYaxis.fXmin) /
      histogram.fYaxis.fNbins
    // bin shift in the scene
    this.#labels.xInitial = histogram.fXaxis.fXmin
    this.#labels.yInitial = histogram.fYaxis.fXmin

    this.#xOffset = this.#checkOffsetValue(this.#xOffset, this.#xLimit)
    this.#yOffset = this.#checkOffsetValue(this.#yOffset, this.#yLimit)

    // define third dimension for th3 histogram
    if (histogram._typename.includes('TH3')) {
      // set current limits
      this.#zLimit = histogram.fZaxis.fNbins
      // bin scales
      this.#zWidth = histogram.fZaxis.fXmax / histogram.fZaxis.fNbins
      // bin shift in the scene
      this.#labels.zInitial = histogram.fZaxis.fXmin

      this.#zOffset = this.#checkOffsetValue(this.#zOffset, this.#zLimit)
    }
  }

  /**
   * Nastaví range pre požet zobrazených binoch v danom rozmere
   */
  setRange(range) {
    if (range) {
      if (range < 4) this.#range = 4
      this.#range = range
    }
  }

  /**
   * Nastaví tému pre vizualizáciu
   */
  setTheme(theme) {
    this.#themeProvider = new ThemeProvider(theme)
  }

  /**
   * Vracia atribút objektu offset x.
   * @return {number} offsetX - Offset na osi x
   */
  getXOffset = () => {
    return this.#xOffset
  }

  /**
   * Vracia atribút objektu offset y.
   * @return {number} offsetX - Offset na osi y
   */
  getYOffset = () => {
    return this.#yOffset
  }

  /**
   * Vracia atribút objektu offset y.
   * @return {number} offsetX - Offset na osi y
   */
  getZOffset = () => {
    return this.#zOffset
  }

  /**
   * Vracia atribút objektu range.
   * @return {number} range - Vracia rozsah sekcie
   */
  getRange = () => {
    return this.#range
  }

  /**
   * Funkcia overí korektnosť hodnoty offsetu.
   * Porovná aktuálny upravený offset s limitom na danej osi.
   * @param {number} offsetValue - Hodnota offsetu, ktorú je potrebné overiť
   * @param {number} limit - Hodnota offsetu, ktorá predstavuje limit pre danú os.
   * @return {number} currOffset - Vracia upravený offset
   */
  #checkOffsetValue = (offsetValue, limit) => {
    let currOffset =
      offsetValue > limit - this.#range ? limit - this.#range : offsetValue

    if (currOffset < 1) {
      currOffset = 1
    }
    return currOffset
  }

  /**
   * Funkcia overí stav binu.
   * @param {string} binId - Identifikátor binu, ktorý je potrebné overiť
   * @return {boolean} state - Vracia True ak je bin v poli označených binov, True ak nie je v poli označených binov
   */
  #checkBinState = (binId) => {
    const updatedArray = this.#selectedBins.filter((item) => item.id === binId)
    return updatedArray.length !== 0
  }

  /**
   * Funkcia vytvorí entitu reprezentujúcu označenie na osi.
   * @param {string} id - Identifikátor, ktorým bude disponovať vytvorená entita
   * @param {number} positionX - X pozícia entity
   * @param {number} positionY - Y pozícia entity
   * @param {number} positionZ - Z pozícia entity
   * @param {number} rotationX - X rotácia entity
   * @param {number} rotationY - Y rotácia entity
   * @param {number} rotationZ - Z rotácia entity
   * @param {number} width - Šírka entity
   * @param {number} height - Výška entity
   * @param {number} value - Hodnota definujúca časové omeškanie pre zobrazenie označenia v entite
   * @param {string} color - HEX hodnota farby entity
   * @param {string} fontColor - HEX hodnota farby fontu entity
   * @return {HTMLElement} entita - Vytvorená entita
   */
  #createAxisEntity = (
    id,
    positionX,
    positionY,
    positionZ,
    rotationX,
    rotationY,
    rotationZ,
    width,
    height,
    value,
    color,
    fontColor
  ) => {
    return {
      key: id + this.#id,
      geometry: {
        primitive: 'plane',
        width: width,
        height: height
      },
      text: {
        color: fontColor,
        width: 4,
        height: 'auto'
      },
      labelHandler: {
        value: value,
        delay: 500
      },
      rotation: {
        x: rotationX,
        y: rotationY,
        z: rotationZ
      },
      material: {
        color: color
      },
      animation: {
        fromX: positionX + this.#shift.xOffset,
        fromY: positionY + this.#shift.zOffset,
        fromZ: positionZ + this.#shift.yOffset,
        toX: positionX,
        toY: positionY,
        toZ: positionZ
      }
    } /* (
      <a-entity
        key={id + this.#id}
        geometry={`primitive: plane; width:${width}; height:${height};`}
        text={`color: ${fontColor}; width: 4; height:auto; align:center;`}
        label-handler={`value: ${value}; delay:${500}`}
        rotation={`${rotationX} ${rotationY} ${rotationZ}`}
        // position={`${positionX} ${positionY} ${positionZ}`}
        material={`color: ${color};`}
        animation='property: text.width; from: 0; to: 4; dur: 1000;'
        animation__1='property: material.opacity; from: 0; to: 0.8; dur: 1500;'
        animation__2={`property: position; from: ${
          positionX + this.#shift.xOffset
        } ${positionY + this.#shift.zOffset} ${
          positionZ + this.#shift.yOffset
        }; to: ${positionX} ${positionY} ${positionZ}; dur: 1005;`}
      />
    ) */
  }

  /**
   * Funkcia na základe atribútov objektu vytvorí všetky potrebné entity pre histogram TH3.
   * @return {Object} elements - Vracia objekt obsahujúci roztriedené všetky potrebné entity histogramu pre VR
   */
  #createTH3Histogram = () => {
    // get selected bins from local storage
    this.#selectedBins = localStorageService.getBinsFromLocalStorage()

    // react elements to draw the histograms
    const elements = {
      type: 'TH3',
      bins: [],
      labels: [],
      banners: [],
      ground: {},
      titles: [],
      axisLabels: []
    }
    // variables for rendering the histogram
    let xcenter, xmin, xmax, xwidth
    let ycenter, ymin, ymax, ywidth
    let zcenter, zmin, zmax, zwidth
    const binName = ''
    let absoluteContent
    let c
    let count = 0

    // load th3 limits from localstorage
    // const binScale = ndmVrStorage.loadBinScale('TH3')
    // console.log(binScale)

    const maxXLength =
      this.#xOffset + this.#range > this.#xLimit
        ? this.#xLimit
        : this.#xOffset + this.#range
    const maxYLength =
      this.#yOffset + this.#range > this.#yLimit
        ? this.#yLimit
        : this.#yOffset + this.#range
    const maxZLength =
      this.#zOffset + this.#range > this.#zLimit
        ? this.#zLimit
        : this.#zOffset + this.#range

    const xTitle = this.#histogram.fXaxis.fTitle
    const yTitle = this.#histogram.fYaxis.fTitle
    const zTitle = this.#histogram.fZaxis.fTitle
    const xLabels = this.#histogram.fXaxis.fLabels
      ? this.#histogram.fXaxis.fLabels.arr
      : []
    const yLabels = this.#histogram.fYaxis.fLabels
      ? this.#histogram.fYaxis.fLabels.arr
      : []
    const zLabels = this.#histogram.fZaxis.fLabels
      ? this.#histogram.fZaxis.fLabels.arr
      : []

    const currentBinScales = []

    // generate react elements of the bins
    for (let iz = this.#zOffset; iz <= maxZLength; iz++)
      for (let iy = this.#yOffset; iy <= maxYLength; iy++)
        for (let ix = this.#xOffset; ix <= maxXLength; ix++) {
          xcenter = this.#histogram.fXaxis.GetBinCenter(ix)
          xmin = this.#histogram.fXaxis.GetBinLowEdge(ix)
          xmax = xcenter + (xcenter - xmin)
          xwidth = xmax - xmin
          ycenter = this.#histogram.fYaxis.GetBinCenter(iy)
          ymin = this.#histogram.fYaxis.GetBinLowEdge(iy)
          ymax = ycenter + (ycenter - ymin)
          ywidth = ymax - ymin
          zcenter = this.#histogram.fZaxis.GetBinCenter(iz)
          zmin = this.#histogram.fZaxis.GetBinLowEdge(iz)
          zmax = zcenter + (zcenter - zmin)
          zwidth = zmax - zmin
          absoluteContent = this.#histogram.getBinContent(ix, iy, iz)
          c = this.#optimizeBinContent(
            absoluteContent,
            this.#histogram.fMaximum
          )

          // set a-frame entity box
          // set children component
          if (c > this.#minDisplayedContent) {
            let size = c
            if (c < 0.15) size = 0.15

            const color = this.#themeProvider.getBinColor(c)
            // check bin status
            const id =
              this.#id +
              xmin +
              'th3' +
              xmax +
              'th3' +
              zmin +
              'th3' +
              zmax +
              'th3' +
              ymin +
              'th3' +
              ymax
            let markedColor
            if (this.#checkBinState(id)) {
              markedColor = this.#themeProvider.getSecondaryAccentColor()
            } else {
              markedColor = color
            }
            // fill bin data
            const binData = `id: ${id};
              typeName: 'TH2'
              content: ${c};
              color: ${color};
              yTitle: ${yTitle};
              xTitle: ${xTitle};
              zTitle: ${zTitle};
              absoluteContent: ${absoluteContent};
              binName: ${binName};
              xMin: ${xmin};
              yMin: ${ymin};
              zMin: ${zmin};
              xMax: ${xmax};
              yMax: ${ymax};
              zMax: ${zmax};
              xPos: ${ix};
              yPos: ${iy};
              zPos: ${iz};
              xCenter: ${xcenter};
              yCenter: ${ycenter};
              zCenter: ${zcenter};
              xWidth: ${xwidth};
              yWidth: ${ywidth};
              zWidth: ${zwidth};
              markedColor: ${markedColor};
              selectColor: ${this.#themeProvider.getSecondaryAccentColor()};
              axisX: ${this.#themeProvider.getAxisColor('x')};
              axisY: ${this.#themeProvider.getAxisColor('y')};
              axisZ: ${this.#themeProvider.getAxisColor('z')}`

            const width = size * 0.8
            // const posX = (ix - 1) / this.#xWidth
            // const posY = (iz - 1) / this.#zWidth + 0.2
            // const posZ = (iy - 1) / this.#yWidth
            const posX = ix - 1
            const posY = iz - 1
            const posZ = iy - 1
            elements.bins[count] = {
              id: id,
              color: color,
              binData: binData,
              animation: {
                fromWidth: this.#getPreviousBinScales(id),
                fromHeight: this.#getPreviousBinScales(id),
                fromDepth: this.#getPreviousBinScales(id),
                toWidth: width,
                toHeight: width,
                toDepth: width
              },
              animation2: {
                fromX: posX + this.#shift.xOffset,
                fromY: posY + this.#shift.zOffset,
                fromZ: posZ + this.#shift.yOffset,
                toX: posX,
                toY: posY,
                toZ: posZ
              },
              animation3: {
                fromColor: this.#getPreviousBinColor(id),
                toColor: markedColor
              }
            }
            /* (
              <a-box
                key={id}
                class='clickable'
                material={`color: ${markedColor};`}
                binth3={binData}
                // position={`${posX} ${posY} ${posZ}`}
                // height={width}
                // width={width}
                // depth={width}
                mouseevent
                animation={`property: scale; from: ${this.#getPreviousBinScales(
                  id
                )} ${this.#getPreviousBinScales(
                  id
                )} ${this.#getPreviousBinScales(
                  id
                )}; to: ${width} ${width} ${width}; dur: 1500; easing: linear;`}
                // animation={`property: scale; to: ${width} ${width} ${width}; dur: 1500; easing: linear;`}
                animation__1='property: material.opacity; from: 0; to: 0.75; dur: 2600;'
                animation__2={`property: position; from: ${
                  posX + this.#shift.xOffset
                } ${posY + this.#shift.zOffset} ${
                  posZ + this.#shift.yOffset
                }; to: ${posX} ${posY} ${posZ}; dur: 1000;`}
                animation__3={`property: material.color; from: ${this.#getPreviousBinColor(
                  id
                )}; to: ${markedColor}; dur: 2600;`}
              />
            )
            */
            currentBinScales[count] = {
              key: id,
              scale: width,
              color: markedColor
            }
          }
          count++
        }

    this.#previousBins = currentBinScales
    // create arrays for labels
    const normalAxis = []
    const reversedAxis = []
    // set axis
    count = 0
    for (let iz = this.#yOffset; iz <= maxYLength; iz++) {
      normalAxis[count] = this.#createAxisEntity(
        50000 + count,
        -2.5 + this.#xOffset,
        this.#zOffset - 1,
        iz - 1,
        270,
        180,
        0,
        2,
        1,
        `${
          yLabels[iz - 1]
            ? yLabels[iz - 1].fString
            : Math.round(
                (this.#labels.yInitial + iz - 1 + Number.EPSILON) * 100
              ) / 100
        }`,
        this.#themeProvider.getAxisColor('z'),
        this.#themeProvider.getSecondaryFontColor()
      )
      reversedAxis[count] = this.#createAxisEntity(
        55000 + count,
        maxXLength + 0.5,
        this.#zOffset - 1,
        iz - 1,
        270,
        360,
        0,
        2,
        1,
        `${
          yLabels[iz - 1]
            ? yLabels[iz - 1].fString
            : Math.round(
                (this.#labels.yInitial + iz - 1 + Number.EPSILON) * 100
              ) / 100
        }`,
        this.#themeProvider.getAxisColor('z'),
        this.#themeProvider.getSecondaryFontColor()
      )
      count++
    }
    // add z label
    elements.axisLabels[0] = {
      id: 'leftLabel',
      key: 6000 + this.#id,
      text: {
        value: yTitle,
        color: this.#themeProvider.getAxisColor('z')
      },
      rotation: {
        x: 270,
        y: 270,
        z: 0
      },
      animation: {
        fromX: this.#xOffset - 4 + this.#shift.xOffset,
        fromY: this.#zOffset - 1 + this.#shift.zOffset,
        fromZ: this.#yOffset + 0.5 + this.#shift.yOffset,
        toX: this.#xOffset - 4,
        toY: this.#zOffset - 1,
        toZ: this.#yOffset + 0.5
      }
    } /* (
      <a-entity
        id='leftLabel'
        key={60000 + count + this.#id}
        geometry='primitive: plane; width:4; height:1;'
        text={`value: ${yTitle}; color: ${this.#themeProvider.getAxisColor(
          'z'
        )}; width: 10; height:auto; align:center;`}
        rotation={`${270} ${270} ${0}`}
        /* position={`${this.#xOffset - 4} ${this.#zOffset - 1} ${
          this.#yOffset + 0.5
        }`}
        animation={`property: position; from: ${
          this.#xOffset - 4 + this.#shift.xOffset
        } ${this.#zOffset - 1 + this.#shift.zOffset} ${
          this.#yOffset + 0.5 + this.#shift.yOffset
        }; to: ${this.#xOffset - 4} ${this.#zOffset - 1} ${
          this.#yOffset + 0.5
        }; dur: 1000;`}
        material='opacity: 0;'
      />
    ) */
    elements.axisLabels[1] = {
      id: 'rightLabel',
      key: 64001 + this.#id,
      text: {
        value: yTitle,
        color: this.#themeProvider.getAxisColor('z')
      },
      rotation: {
        x: 270,
        y: 90,
        z: 0
      },
      animation: {
        fromX: maxXLength + 2 + this.#shift.xOffset,
        fromY: this.#zOffset - 1 + this.#shift.zOffset,
        fromZ: maxYLength - 2.5 + this.#shift.yOffset,
        toX: maxXLength + 2,
        toY: this.#zOffset - 1,
        toZ: maxYLength - 2.5
      }
    } /* (
      <a-entity
        id='rightLabel'
        key={65000 + count + this.#id}
        geometry='primitive: plane; width:4; height:1;'
        text={`value: ${yTitle}; color: ${this.#themeProvider.getAxisColor(
          'z'
        )}; width: 10; height:auto; align:center;`}
        rotation={`${270} ${90} ${0}`}
        // position={`${maxXLength + 2} ${this.#zOffset - 1} ${maxYLength - 2.5}`}
        animation={`property: position; from: ${
          maxXLength + 2 + this.#shift.xOffset
        } ${this.#zOffset - 1 + this.#shift.zOffset} ${
          maxYLength - 2.5 + this.#shift.yOffset
        }; to: ${maxXLength + 2} ${this.#zOffset - 1} ${
          maxYLength - 2.5
        }; dur: 1000;`}
        material='opacity: 0;'
      />
    ) */
    // generate x and y axis
    for (let ix = this.#xOffset; ix <= maxXLength; ix++) {
      normalAxis[count] = this.#createAxisEntity(
        70000 + count,
        ix - 1,
        this.#zOffset - 1,
        this.#yOffset - 2.5,
        270,
        270,
        0,
        2,
        1,
        `${
          xLabels[ix - 1]
            ? xLabels[ix - 1].fString
            : Math.round(
                (this.#labels.xInitial + ix - 1 + Number.EPSILON) * 100
              ) / 100
        }`,
        this.#themeProvider.getAxisColor('x'),
        this.#themeProvider.getSecondaryFontColor()
      )
      reversedAxis[count] = this.#createAxisEntity(
        75000 + count,
        ix - 1,
        this.#zOffset - 1,
        maxYLength + 0.5,
        270,
        90,
        0,
        2,
        1,
        `${
          xLabels[ix - 1]
            ? xLabels[ix - 1].fString
            : Math.round(
                (this.#labels.xInitial + ix - 1 + Number.EPSILON) * 100
              ) / 100
        }`,
        this.#themeProvider.getAxisColor('x'),
        this.#themeProvider.getSecondaryFontColor()
      )
      count++
    }
    // add x label
    elements.axisLabels[2] = {
      id: 'downLabel',
      key: 84002 + this.#id,
      text: {
        value: xTitle,
        color: this.#themeProvider.getAxisColor('x')
      },
      rotation: {
        x: 270,
        y: 180,
        z: 0
      },
      animation: {
        fromX: this.#xOffset + this.#shift.xOffset,
        fromY: this.#zOffset - 1 + this.#shift.zOffset,
        fromZ: this.#yOffset - 4 + this.#shift.yOffset,
        toX: this.#xOffset,
        toY: this.#zOffset - 1,
        toZ: this.#yOffset - 4
      }
    } /* (
      <a-entity
        id='downLabel'
        key={80000 + count + this.#id}
        geometry='primitive: plane; width:4; height:1;'
        text={`value: ${xTitle}; color: ${this.#themeProvider.getAxisColor(
          'x'
        )}; width: 10; height:auto; align:center;`}
        rotation={`${270} ${180} ${0}`}
        // position={`${this.#xOffset} ${this.#zOffset - 1} ${this.#yOffset - 4}`}
        animation={`property: position; from: ${
          this.#xOffset + this.#shift.xOffset
        } ${this.#zOffset - 1 + this.#shift.zOffset} ${
          this.#yOffset - 4 + this.#shift.yOffset
        }; to: ${this.#xOffset} ${this.#zOffset - 1} ${
          this.#yOffset - 4
        }; dur: 1000;`}
        material='opacity: 0;'
      />
    ) */
    elements.axisLabels[3] = {
      id: 'upLabel',
      key: 85003 + this.#id,
      text: {
        value: xTitle,
        color: this.#themeProvider.getAxisColor('x')
      },
      rotation: {
        x: 270,
        y: 0,
        z: 0
      },
      animation: {
        fromX: maxXLength - 2.5 + this.#shift.xOffset,
        fromY: this.#zOffset - 1 + this.#shift.zOffset,
        fromZ: maxYLength + 2 + this.#shift.yOffset,
        toX: maxXLength - 2.5,
        toY: this.#zOffset - 1,
        toZ: maxYLength + 2
      }
    } /* (
      <a-entity
        id='upLabel'
        key={85000 + count + this.#id}
        geometry='primitive: plane; width:4; height:1;'
        text={`value: ${xTitle}; color: ${this.#themeProvider.getAxisColor(
          'x'
        )}; width: 10; height:auto; align:center;`}
        rotation={`${270} ${0} ${0}`}
        // position={`${maxXLength - 2.5} ${this.#zOffset - 1} ${maxYLength + 2}`}
        animation={`property: position; from: ${
          maxXLength - 2.5 + this.#shift.xOffset
        } ${this.#zOffset - 1 + this.#shift.zOffset} ${
          maxYLength + 2 + this.#shift.yOffset
        }; to: ${maxXLength - 2.5} ${this.#zOffset - 1} ${
          maxYLength + 2
        }; dur: 1000;`}
        material='opacity: 0;'
      />
    ) */
    // y axis
    normalAxis[count] = this.#createAxisEntity(
      90000 + count,
      maxXLength + 0.5,
      this.#zOffset - 1,
      maxYLength + 0.5,
      270,
      360,
      0,
      2,
      2,
      `${
        zLabels[this.#zOffset - 1]
          ? zLabels[this.#zOffset - 1].fString
          : Math.round(
              (this.#labels.zInitial + this.#zOffset - 1 + Number.EPSILON) * 100
            ) / 100
      }`,
      this.#themeProvider.getAxisColor('y'),
      this.#themeProvider.getSecondaryFontColor()
    )
    reversedAxis[count] = this.#createAxisEntity(
      91000 + count,
      maxXLength + 0.5,
      this.#zOffset - 1,
      this.#yOffset - 2.5,
      270,
      90,
      0,
      2,
      2,
      `${
        zLabels[this.#zOffset - 1]
          ? zLabels[this.#zOffset - 1].fString
          : Math.round(
              (this.#labels.zInitial + this.#zOffset - 1 + Number.EPSILON) * 100
            ) / 100
      }`,
      this.#themeProvider.getAxisColor('y'),
      this.#themeProvider.getSecondaryFontColor()
    )
    count++
    normalAxis[count] = this.#createAxisEntity(
      92000 + count,
      this.#xOffset - 2.5,
      this.#zOffset - 1,
      maxYLength + 0.5,
      270,
      270,
      0,
      2,
      2,
      `${
        zLabels[this.#zOffset - 1]
          ? zLabels[this.#zOffset - 1].fString
          : Math.round(
              (this.#labels.zInitial + this.#zOffset - 1 + Number.EPSILON) * 100
            ) / 100
      }`,
      this.#themeProvider.getAxisColor('y'),
      this.#themeProvider.getSecondaryFontColor()
    )
    reversedAxis[count] = this.#createAxisEntity(
      93000 + count,
      this.#xOffset - 2.5,
      this.#zOffset - 1,
      this.#yOffset - 2.5,
      270,
      180,
      0,
      2,
      2,
      `${
        zLabels[this.#zOffset - 1]
          ? zLabels[this.#zOffset - 1].fString
          : Math.round(
              (this.#labels.zInitial + this.#zOffset - 1 + Number.EPSILON) * 100
            ) / 100
      }`,
      this.#themeProvider.getAxisColor('y'),
      this.#themeProvider.getSecondaryFontColor()
    )
    count++
    // add y label
    elements.axisLabels[4] = {
      id: 'upLabel1',
      key: 95004 + this.#id,
      text: {
        value: zTitle,
        color: this.#themeProvider.getAxisColor('y')
      },
      rotation: {
        x: 0,
        y: 270,
        z: 90
      },
      animation: {
        fromX: this.#xOffset - 1.5 + this.#shift.xOffset,
        fromY: maxZLength - 1 + this.#shift.zOffset,
        fromZ: this.#yOffset - 2 + this.#shift.yOffset,
        toX: this.#xOffset - 1.5,
        toY: maxZLength - 1,
        toZ: this.#yOffset - 2
      }
    } /* (
      <a-entity
        key={95000 + count + this.#id}
        text={`value: ${zTitle}; color: ${this.#themeProvider.getAxisColor(
          'y'
        )}; width: 10; height:auto; align:center;`}
        rotation={`${0} ${270} ${90}`}
        /* position={`${this.#xOffset - 1.5} ${maxZLength - 1} ${
          this.#yOffset - 2
        }`}
        animation={`property: position; from: ${
          this.#xOffset - 1.5 + this.#shift.xOffset
        } ${maxZLength - 1 + this.#shift.zOffset} ${
          this.#yOffset - 2 + this.#shift.yOffset
        }; to: ${this.#xOffset - 1.5} ${maxZLength - 1} ${
          this.#yOffset - 2
        }; dur: 1000;`}
        material='opacity: 0;'
      />
    ) */
    elements.axisLabels[5] = {
      id: 'upLabel2',
      key: 100005 + this.#id,
      text: {
        value: zTitle,
        color: this.#themeProvider.getAxisColor('y')
      },
      rotation: {
        x: 0,
        y: 90,
        z: 90
      },
      animation: {
        fromX: maxXLength - 0.5 + this.#shift.xOffset,
        fromY: maxZLength - 1 + this.#shift.zOffset,
        fromZ: maxYLength + this.#shift.yOffset,
        toX: maxXLength - 0.5,
        toY: maxZLength - 1,
        toZ: maxYLength
      }
    } /* (
      <a-entity
        key={100000 + count + this.#id}
        text={`value: ${zTitle}; color: ${this.#themeProvider.getAxisColor(
          'y'
        )}; width: 10; height:auto; align:center;`}
        rotation={`${0} ${90} ${90}`}
        // position={`${maxXLength - 0.5} ${maxZLength - 1} ${maxYLength}`}
        animation={`property: position; from: ${
          maxXLength - 0.5 + this.#shift.xOffset
        } ${maxZLength - 1 + this.#shift.zOffset} ${
          maxYLength + this.#shift.yOffset
        }; to: ${maxXLength - 0.5} ${maxZLength - 1} ${maxYLength}; dur: 1000;`}
        material='opacity: 0;'
      />
    ) */
    elements.axisLabels[6] = {
      id: 'upLabel3',
      key: 105006 + this.#id,
      text: {
        value: zTitle,
        color: this.#themeProvider.getAxisColor('y')
      },
      rotation: {
        x: 0,
        y: 360,
        z: 90
      },
      animation: {
        fromX: this.#xOffset - 2 + this.#shift.xOffset,
        fromY: maxZLength - 1 + this.#shift.zOffset,
        fromZ: maxYLength - 0.5 + this.#shift.yOffset,
        toX: this.#xOffset - 2,
        toY: maxZLength - 1,
        toZ: maxYLength - 0.5
      }
    } /* (
      <a-entity
        key={105000 + count + this.#id}
        text={`value: ${zTitle}; color: ${this.#themeProvider.getAxisColor(
          'y'
        )}; width: 10; height:auto; align:center;`}
        rotation={`${0} ${360} ${90}`}
        // position={`${this.#xOffset - 2} ${maxZLength - 1} ${maxYLength - 0.5}`}
        animation={`property: position; from: ${
          this.#xOffset - 2 + this.#shift.xOffset
        } ${maxZLength - 1 + this.#shift.zOffset} ${
          maxYLength - 0.5 + this.#shift.yOffset
        }; to: ${this.#xOffset - 2} ${maxZLength - 1} ${
          maxYLength - 0.5
        }; dur: 1000;`}
        material='opacity: 0;'
      />
    ) */
    elements.axisLabels[7] = {
      id: 'upLabel4',
      key: 105007 + this.#id,
      text: {
        value: zTitle,
        color: this.#themeProvider.getAxisColor('y')
      },
      rotation: {
        x: 0,
        y: 180,
        z: 90
      },
      animation: {
        fromX: maxXLength - 0.5 + this.#shift.xOffset,
        fromY: maxZLength - 1 + this.#shift.zOffset,
        fromZ: this.#yOffset - 1.5 + this.#shift.yOffset,
        toX: maxXLength - 0.5,
        toY: maxZLength - 1,
        toZ: this.#yOffset - 1.5
      }
    } /* (
      <a-entity
        key={110000 + count + this.#id}
        text={`value: ${zTitle}; color: ${this.#themeProvider.getAxisColor(
          'y'
        )}; width: 10; height:auto; align:center;`}
        rotation={`${0} ${180} ${90}`}
        /* position={`${maxXLength - 0.5} ${maxZLength - 1} ${
        }`}
        animation={`property: position; from: ${
          maxXLength - 0.5 + this.#shift.xOffset
        } ${maxZLength - 1 + this.#shift.zOffset} ${
          this.#yOffset - 1.5 + this.#shift.yOffset
        }; to: ${maxXLength - 0.5} ${maxZLength - 1} ${
          this.#yOffset - 1.5
        }; dur: 1000;`}
        material='opacity: 0;'
      />
    ) */

    // add histogram title
    elements.titles[0] = {
      key: 150000 + this.#id,
      text: {
        value: this.#histogram.fTitle,
        color: this.#themeProvider.getPrimaryFontColor()
      },
      rotation: {
        x: 0,
        y: 123,
        z: 0
      },
      position: {
        x: this.#xOffset - 90,
        y: maxZLength + 16.5,
        z: this.#yOffset + 90
      },
      innerText: {
        value: 'Press V to show projections or controls',
        color: this.#themeProvider.getPrimaryFontColor()
      }
    } /* (
      <a-entity
        key={115000 + count + this.#id}
        text={`value: ${
          this.#histogram.fTitle
        }; color: ${this.#themeProvider.getPrimaryFontColor()}; width: 270; height:auto; align:center;`}
        rotation={`${0} ${123} ${0}`}
        position={`${this.#xOffset - 90} ${maxZLength + 16.5} ${
          this.#yOffset + 90
        }`}
        // material='opacity: 0.5; color: white;'
      >
        <a-text
          value='Press V to show projections or controls'
          position='0 10 0'
          width='150'
          height='auto'
          align='center'
          color={this.#themeProvider.getPrimaryFontColor()}
        />
        <a-entity
          geometry='primitive: plane; width:170; height:30;'
          position='0 0 -1'
          material='opacity: 0.9; color: white;'
        />
      </a-entity>
    ) */
    // add file title
    elements.titles[1] = {
      key: 120001 + this.#id,
      text: {
        value: this.#histogram.fName,
        color: this.#themeProvider.getPrimaryFontColor()
      },
      rotation: {
        x: 0,
        y: 330,
        z: 0
      },
      position: {
        x: this.#xOffset + 120,
        y: maxZLength + 16.5,
        z: this.#yOffset - 70
      },
      innerText: {
        value: 'Press buttonY to show projections or controls',
        color: this.#themeProvider.getPrimaryFontColor()
      }
    } /* (
      <a-entity
        key={120000 + count + this.#id}
        // geometry='primitive: plane; width:170; height:30;'
        text={`value: ${
          this.#histogram.fName
        }; color: ${this.#themeProvider.getPrimaryFontColor()}; width: 230; height:auto; align:center;`}
        rotation={`${0} ${330} ${0}`}
        position={`${this.#xOffset + 120} ${maxZLength + 16.5} ${
          this.#yOffset - 70
        }`}
        // material='opacity: 0.5; color: white;'
      >
        <a-text
          value='Press buttonY to show projections or controls'
          position='0 10 0'
          width='150'
          height='auto'
          align='center'
          color={this.#themeProvider.getPrimaryFontColor()}
        />
        <a-entity
          geometry='primitive: plane; width:170; height:30;'
          position='0 0 -1'
          material='opacity: 0.9; color: white;'
        />
      </a-entity>
    ) */
    count++
    const bannerWidthX =
      this.#range > this.#xLimit ? this.#xLimit + 1 : this.#range
    const bannerWidthY =
      this.#range > this.#yLimit ? this.#yLimit + 1 : this.#range
    elements.ground = {
      key: 125000 + count + this.#id,
      scale: {
        width: bannerWidthX + 8,
        height: 0.2,
        depth: bannerWidthY + 8
      },
      animation: {
        fromX: this.#xOffset + bannerWidthX * 0.5 - 1 + this.#shift.xOffset,
        fromY: this.#zOffset - 1.5 + this.#shift.zOffset,
        fromZ: this.#yOffset + bannerWidthY * 0.5 - 0.5 + this.#shift.yOffset,
        toX: this.#xOffset + bannerWidthX * 0.5 - 1,
        toY: this.#zOffset - 1.5,
        toZ: this.#yOffset + bannerWidthY * 0.5 - 0.5
      }
    } /* (
      <a-box
        key={125000 + count + this.#id}
        width={bannerWidthX + 8}
        material='color: #171717'
        height={0.2}
        depth={bannerWidthY + 8}
        // position={`${this.#xOffset + 2.5} -0.5 ${this.#yOffset + 2.5}`}
        animation={`property: position; from: ${
          this.#xOffset + bannerWidthX * 0.5 - 1 + this.#shift.xOffset
        } ${this.#zOffset - 1.5 + this.#shift.zOffset}
        ${this.#yOffset + bannerWidthY * 0.5 - 0.5 + this.#shift.yOffset};
        to: ${this.#xOffset + bannerWidthX * 0.5 - 1} ${this.#zOffset - 1.5} ${
          this.#yOffset + bannerWidthY * 0.5 - 0.5
        }dur: 1100;`}
      />
    ) */
    // add banner
    /*
    elements.banners[0] = (
      <a-entity
        key={4500 + count + 2}
        id='th3-banner'
        text={`value: ""; color: white; width: 230; height:auto; align:center;`}
        // rotation={`${0} ${330} ${0}`}
        position={`${this.#xOffset + this.#range / 2 + 2.5} ${
          this.#zOffset + this.#range / 2 + 1.5
        } ${maxYLength + 8}`}
      >
        <a-entity rotation='0 180 0' position='0 6 0' text={`value: "";`} />
        <a-entity rotation='0 180 0' position='0 4 0' text={`value: "";`} />
        <a-entity rotation='0 180 0' position='0 2 0' text={`value: "";`} />
        <a-entity rotation='0 180 0' position='0 0 0' text={`value: "";`} />
        <a-entity
          geometry='primitive: box; width:15; height:15; depth:0.1;'
          position='0 0 0.5'
          material='color: black;'
        />
      </a-entity>
    )
     */
    // add banner with mappings
    count++
    elements.banners[1] = {
      key: 130000 + count + this.#id,
      id: 'th-mapping',
      geometry: {
        width: 1,
        height: 15,
        depth: 22
      },
      position: {
        x: maxXLength + 8,
        y: this.#zOffset + this.#range / 2 + 0.5,
        z: this.#yOffset + this.#range / 2 + 2.5
      },
      material: {
        color: '#ffffff',
        transparent: true
      }
    } /* (
      <a-box
        key={130000 + count + this.#id}
        id='th-mapping'
        geometry='width:1; height:15; depth:22;'
        // text={`value: ${histogram.fName}; color: black; width: 230; height:auto; align:center;`}
        // rotation={`${0} ${330} ${0}`}
        position={`${maxXLength + 8} ${this.#zOffset + this.#range / 2 + 0.5}
       ${this.#yOffset + this.#range / 2 + 2.5}`}
        material='transparent:true'
      />
    ) */
    count++
    elements.banners[2] = {
      key: 135000 + count + this.#id,
      id: 'th-background',
      geometry: {
        width: 0.2,
        height: 15,
        depth: 24
      },
      position: {
        x: maxXLength + 8.2,
        y: this.#zOffset + this.#range / 2 + 0.5,
        z: this.#yOffset + 1 + this.#range / 2 + 1.5
      },
      material: {
        color: '#ffffff',
        transparent: false
      }
    } /* (
      <a-box
        key={135000 + count + this.#id}
        id='th-background'
        geometry='width:0.2; height:15; depth:24;'
        // text={`value: ${histogram.fName}; color: black; width: 230; height:auto; align:center;`}
        // rotation={`${0} ${330} ${0}`}
        position={`${maxXLength + 8.2} ${this.#zOffset + this.#range / 2 + 0.5}
       ${this.#yOffset + 1 + this.#range / 2 + 1.5}`}
        material='color: white, transparent:false'
      />
    ) */
    // add normal and reversed labels into elements
    elements.labels = [...normalAxis, ...reversedAxis]

    return elements
  }

  /**
   * Funkcia na základe atribútov objektu vytvorí všetky potrebné entity pre histogram TH2.
   * @param {string} projectionFunction - parameter určuje, ktorý vizualizačný mód bude použity pri vytvorení entít
   * @return {Object} elements - vracia objekt obsahujúci roztriedené všetky potrebné entity histogramu pre VR
   */
  #createTH2Histogram = (projectionFunction) => {
    // get selected bins from local storage
    this.#selectedBins = localStorageService.getBinsFromLocalStorage()

    // react elements to draw the histogram
    const elements = {
      type: 'TH2',
      bins: [],
      labels: [],
      banners: [],
      ground: {},
      titles: [],
      axisLabels: []
    }
    // variables for rendering the histogram
    let centeredYPosition
    let xcenter, xmin, xmax, xwidth
    let ycenter, ymin, ymax, ywidth
    let c
    const binName = ''
    let absoluteContent
    let count = 0
    // bin attributes
    // const binScale = ndmVrStorage.loadBinScale('TH2')
    // max length
    const maxXLength =
      this.#xOffset + this.#range > this.#histogram.fXaxis.fNbins
        ? this.#histogram.fXaxis.fNbins
        : this.#xOffset + this.#range
    const maxYLength =
      this.#yOffset + this.#range > this.#histogram.fYaxis.fNbins
        ? this.#histogram.fYaxis.fNbins
        : this.#yOffset + this.#range
    const xTitle =
      this.#histogram.fXaxis.fTitle !== '' ? this.#histogram.fXaxis.fTitle : 'x'
    const yTitle =
      this.#histogram.fYaxis.fTitle !== '' ? this.#histogram.fYaxis.fTitle : 'y'
    const xLabels = this.#histogram.fXaxis.fLabels
      ? this.#histogram.fXaxis.fLabels.arr
      : []
    const yLabels = this.#histogram.fYaxis.fLabels
      ? this.#histogram.fYaxis.fLabels.arr
      : []
    // reset previous bins
    const currentBinScales = []

    // generate react elements of the bins
    for (let iy = this.#yOffset; iy <= maxYLength; iy++) {
      for (let ix = this.#xOffset; ix <= maxXLength; ix++) {
        xcenter = this.#histogram.fXaxis.GetBinCenter(ix)
        xmin = this.#histogram.fXaxis.GetBinLowEdge(ix)
        xmax = xcenter + (xcenter - xmin)
        xwidth = xmax - xmin
        ycenter = this.#histogram.fYaxis.GetBinCenter(iy)
        ymin = this.#histogram.fYaxis.GetBinLowEdge(iy)
        ymax = ycenter + (ycenter - ymin)
        ywidth = ymax - ymin
        absoluteContent = this.#histogram.getBinContent(ix, iy)
        c = this.#optimizeBinContent(absoluteContent, this.#histogram.fMaximum)
        centeredYPosition = (c * this.#range * this.#contentScale) / 16

        // set a-frame entity box
        // set children component
        if (c > this.#minDisplayedContent) {
          const projectionColor = this.#projectionFunction(
            projectionFunction,
            xmin,
            ymin
          )
          const color =
            projectionColor === ''
              ? this.#themeProvider.getBinColor(c)
              : projectionColor

          // check bin status
          const id =
            this.#id + xmin + 'th2' + xmax + 'th2' + ymin + 'th2' + ymax
          let markedColor
          if (this.#checkBinState(id)) {
            markedColor = this.#themeProvider.getSecondaryAccentColor()
          } else {
            markedColor = color
          }
          // fill bin data
          const binData = `id: ${id};
            typeName: 'TH2'
            content: ${c};
            color: ${color};
            yTitle: ${yTitle};
            xTitle: ${xTitle};
            zTitle: 'none';
            absoluteContent: ${absoluteContent};
            binName: ${binName};
            xMin: ${xmin};
            yMin: ${ymin};
            zMin: ${0};
            xMax: ${xmax};
            yMax: ${ymax};
            zMax: ${0};
            xPos: ${ix};
            yPos: ${iy};
            zPos: ${0};
            xCenter: ${xcenter};
            yCenter: ${ycenter};
            yCenter: ${0};
            xWidth: ${xwidth};
            yWidth: ${ywidth};
            zWidth: ${0};
            markedColor: ${markedColor};
            selectColor: ${this.#themeProvider.getSecondaryAccentColor()};
            axisX: ${this.#themeProvider.getAxisColor('x')};
            axisY: ${this.#themeProvider.getAxisColor('y')};
            axisZ: ${this.#themeProvider.getAxisColor('z')}`

          const width = xwidth / this.#xWidth / this.#binScaleCoef
          const depth = ywidth / this.#yWidth / this.#binScaleCoef
          const posX = ix - 1
          const posY = iy - 1
          elements.bins[count] = {
            id: id,
            color: color,
            binData: binData,
            animation: {
              fromWidth: width,
              fromHeight: this.#getPreviousBinScales(id),
              fromDepth: depth,
              toWidth: width,
              toHeight: (c * this.#range * this.#contentScale) / 8,
              toDepth: depth
            },
            animation2: {
              fromX: posX + this.#shift.xOffset,
              fromY: centeredYPosition,
              fromZ: posY + this.#shift.yOffset,
              toX: posX,
              toY: centeredYPosition,
              toZ: posY
            },
            animation3: {
              fromColor: this.#getPreviousBinColor(id),
              toColor: markedColor
            }
          }
          /* (
            <a-box
              key={id}
              class='clickable'
              material={`color: ${markedColor}; opacity: 0.75;`}
              binth2={binData}
              // position={`${posX} ${centeredYPosition} ${posY}`}
              mouseevent
              animation={`property: scale; from: ${width} ${this.#getPreviousBinScales(
                id
              )} ${depth}; to: ${width} ${c} ${depth}; delay:100; dur: 1500; easing: linear;`}
              animation__1='property: material.opacity; from: 0; to: 0.75; dur: 2000;'
              animation__2={`property: position; from: ${
                posX + this.#shift.xOffset
              } ${centeredYPosition} ${
                posY + this.#shift.yOffset
              }; to: ${posX} ${centeredYPosition} ${posY}; dur: 1000;`}
              // animation__3={`property: material.color; from: ${this.#getPreviousBinColor(
              // >/  id
              // )}; to: ${markedColor}; dur: 2600;`}
            />
          ) */
          currentBinScales[count] = {
            key: id,
            scale: c,
            color: markedColor
          }
        }
        count++
      }
    }
    this.#previousBins = currentBinScales
    // create arrays for labels
    const normalAxis = []
    const reversedAxis = []
    // set axis
    count = 0
    for (let iy = this.#yOffset; iy <= maxYLength; iy++) {
      normalAxis[count] = this.#createAxisEntity(
        1500 + count,
        this.#xOffset - 2.5,
        0,
        iy - 1,
        270,
        180,
        0,
        2,
        1,
        `${
          yLabels[iy - 1]
            ? yLabels[iy - 1].fString
            : Math.round(
                (this.#labels.yInitial +
                  (iy - 1) * this.#yWidth +
                  Number.EPSILON) *
                  100
              ) / 100
        }`,
        this.#themeProvider.getAxisColor('z'),
        this.#themeProvider.getSecondaryFontColor()
      )
      reversedAxis[count] = this.#createAxisEntity(
        3000 + count,
        maxXLength + 0.5,
        0,
        iy - 1,
        270,
        360,
        0,
        2,
        1,
        `${
          yLabels[iy - 1]
            ? yLabels[iy - 1].fString
            : Math.round(
                (this.#labels.yInitial +
                  (iy - 1) * this.#yWidth +
                  Number.EPSILON) *
                  100
              ) / 100
        }`,
        this.#themeProvider.getAxisColor('z'),
        this.#themeProvider.getSecondaryFontColor()
      )
      // axis title
      count++
    }
    // add y label
    elements.axisLabels[0] = {
      id: 'leftLabel',
      key: 4500 + this.#id,
      text: {
        value: yTitle,
        color: this.#themeProvider.getAxisColor('z')
      },
      rotation: {
        x: 270,
        y: 270,
        z: 0
      },
      animation: {
        fromX: this.#xOffset - 4 + this.#shift.xOffset,
        fromY: this.#shift.zOffset,
        fromZ: this.#yOffset + 0.5 + this.#shift.yOffset,
        toX: this.#xOffset - 4,
        toY: 0,
        toZ: this.#yOffset + 0.5
      }
    } /* (
      <a-entity
        id='leftLabel'
        key={4500 + count + this.#id}
        geometry='primitive: plane; width:4; height:1;'
        text={`value: ${yTitle}; color: ${this.#themeProvider.getAxisColor(
          'z'
        )}; width: 10; height:auto; align:center;`}
        rotation={`${270} ${270} ${0}`}
        // position={`${this.#xOffset - 4} ${0} ${this.#yOffset + 0.5}`}
        animation={`property: position; from: ${
          this.#xOffset - 4 + this.#shift.xOffset
        } ${this.#shift.zOffset} ${
          this.#yOffset + 0.5 + this.#shift.yOffset
        }; to: ${this.#xOffset - 4} ${0} ${this.#yOffset + 0.5}; dur: 1000;`}
        material='opacity: 0;'
      />
    ) */
    elements.axisLabels[1] = {
      id: 'rightLabel',
      key: 6001 + this.#id,
      text: {
        value: yTitle,
        color: this.#themeProvider.getAxisColor('z')
      },
      rotation: {
        x: 270,
        y: 90,
        z: 0
      },
      animation: {
        fromX: maxXLength + 2 + this.#shift.xOffset,
        fromY: this.#shift.zOffset,
        fromZ: maxYLength - 2.5 + this.#shift.yOffset,
        toX: maxXLength + 2,
        toY: 0,
        toZ: maxYLength - 2.5
      }
    }
    /* (
      <a-entity
        id='rightLabel'
        key={6000 + count + this.#id}
        geometry='primitive: plane; width:4; height:1;'
        text={`value: ${yTitle}; color: ${this.#themeProvider.getAxisColor(
          'z'
        )}; width: 10; height:auto; align:center;`}
        rotation={`${270} ${90} ${0}`}
        // position={`${maxXLength + 2} ${0} ${maxYLength - 2.5}`}
        animation={`property: position; from: ${
          maxXLength + 2 + this.#shift.xOffset
        } ${this.#shift.zOffset} ${
          maxYLength - 2.5 + this.#shift.yOffset
        }; to: ${maxXLength + 2} ${0} ${maxYLength - 2.5}; dur: 1000;`}
        material='opacity: 0;'
      />
    ) */
    // generate x axis
    for (let ix = this.#xOffset; ix <= maxXLength; ix++) {
      normalAxis[count] = this.#createAxisEntity(
        7500 + count,
        ix - 1,
        0,
        this.#yOffset - 2.5,
        270,
        270,
        0,
        2,
        1,
        `${
          xLabels[ix - 1]
            ? xLabels[ix - 1].fString
            : Math.round(
                (this.#labels.xInitial +
                  (ix - 1) * this.#xWidth +
                  Number.EPSILON) *
                  100
              ) / 100
        }`,
        this.#themeProvider.getAxisColor('x'),
        this.#themeProvider.getSecondaryFontColor()
      )
      reversedAxis[count] = this.#createAxisEntity(
        9000 + count,
        ix - 1,
        0,
        maxYLength + 0.5,
        270,
        90,
        0,
        2,
        1,
        `${
          xLabels[ix - 1]
            ? xLabels[ix - 1].fString
            : Math.round(
                (this.#labels.xInitial +
                  (ix - 1) * this.#xWidth +
                  Number.EPSILON) *
                  100
              ) / 100
        }`,
        this.#themeProvider.getAxisColor('x'),
        this.#themeProvider.getSecondaryFontColor()
      )
      count++
    }
    // add x label
    elements.axisLabels[2] = {
      id: 'downLabel',
      key: 10502 + this.#id,
      text: {
        value: xTitle,
        color: this.#themeProvider.getAxisColor('x')
      },
      rotation: {
        x: 270,
        y: 180,
        z: 0
      },
      animation: {
        fromX: this.#xOffset + this.#shift.xOffset,
        fromY: this.#shift.zOffset,
        fromZ: this.#yOffset - 4 + this.#shift.yOffset,
        toX: this.#xOffset,
        toY: 0,
        toZ: this.#yOffset - 4
      }
    } /* (
      <a-entity
        id='downLabel'
        key={10500 + count + this.#id}
        geometry='primitive: plane; width:4; height:1;'
        text={`value: ${xTitle}; color: ${this.#themeProvider.getAxisColor(
          'x'
        )}; width: 10; height:auto; align:center;`}
        rotation={`${270} ${180} ${0}`}
        // position={`${this.#xOffset} ${0} ${this.#yOffset - 4}`}
        animation={`property: position; from: ${
          this.#xOffset + this.#shift.xOffset
        } ${this.#shift.zOffset} ${
          this.#yOffset - 4 + this.#shift.yOffset
        }; to: ${this.#xOffset} ${0} ${this.#yOffset - 4}; dur: 1000;`}
        material='opacity: 0;'
      />
    ) */
    elements.axisLabels[3] = {
      id: 'upLabel',
      key: 12003 + this.#id,
      text: {
        value: xTitle,
        color: this.#themeProvider.getAxisColor('x')
      },
      rotation: {
        x: 270,
        y: 0,
        z: 0
      },
      animation: {
        fromX: maxXLength - 2.5 + this.#shift.xOffset,
        fromY: this.#shift.zOffset,
        fromZ: maxYLength + 2 + this.#shift.yOffset,
        toX: maxXLength - 2.5,
        toY: 0,
        toZ: maxYLength + 2
      }
    } /* (
      <a-entity
        id='upLabel'
        key={12000 + count + this.#id}
        geometry='primitive: plane; width:4; height:1;'
        text={`value: ${xTitle}; color: ${this.#themeProvider.getAxisColor(
          'x'
        )}; width: 10; height:auto; align:center;`}
        rotation={`${270} ${0} ${0}`}
        // position={`${maxXLength - 2.5} ${0} ${maxYLength + 2}`}
        animation={`property: position; from: ${
          maxXLength - 2.5 + this.#shift.xOffset
        } ${this.#shift.zOffset} ${maxYLength + 2 + this.#shift.yOffset}; to: ${
          maxXLength - 2.5
        } ${0} ${maxYLength + 2}; dur: 1000;`}
        material='opacity: 0;'
      />
    ) */

    // generate y axis
    normalAxis[count] = this.#createAxisEntity(
      13500 + count,
      maxXLength + 0.5,
      0,
      maxYLength + 0.5,
      270,
      360,
      0,
      2,
      2,
      `0`,
      this.#themeProvider.getAxisColor('y'),
      this.#themeProvider.getSecondaryFontColor()
    )
    reversedAxis[count] = this.#createAxisEntity(
      15000 + count,
      maxXLength + 0.5,
      0,
      this.#yOffset - 2.5,
      270,
      180,
      0,
      2,
      2,
      `0`,
      this.#themeProvider.getAxisColor('y'),
      this.#themeProvider.getSecondaryFontColor()
    )
    count++
    normalAxis[count] = this.#createAxisEntity(
      16500 + count,
      this.#xOffset - 2.5,
      0,
      maxYLength + 0.5,
      270,
      180,
      0,
      2,
      2,
      `0`,
      this.#themeProvider.getAxisColor('y'),
      this.#themeProvider.getSecondaryFontColor()
    )
    reversedAxis[count] = this.#createAxisEntity(
      18000 + count,
      this.#xOffset - 2.5,
      0,
      this.#yOffset - 2.5,
      270,
      180,
      0,
      2,
      2,
      `0`,
      this.#themeProvider.getAxisColor('y'),
      this.#themeProvider.getSecondaryFontColor()
    )
    // add histogram title
    count++
    elements.titles[0] = {
      key: 195000 + this.#id,
      text: {
        value: this.#histogram.fTitle,
        color: this.#themeProvider.getPrimaryFontColor()
      },
      rotation: {
        x: 0,
        y: 123,
        z: 0
      },
      position: {
        x: this.#xOffset - 90,
        y: 16.5,
        z: this.#yOffset + 90
      },
      innerText: {
        value: 'Press V to show projections or controls',
        color: this.#themeProvider.getPrimaryFontColor()
      }
    } /* (
      <a-entity
        key={19500 + count + this.#id}
        // geometry='primitive: plane; width:170; height:30;'
        text={`value: ${
          this.#histogram.fTitle
        }; color: ${this.#themeProvider.getPrimaryFontColor()}; width: 270; height:auto; align:center;`}
        rotation={`${0} ${123} ${0}`}
        position={`${this.#xOffset - 90} ${16.5} ${this.#yOffset + 90}`}
        // material='opacity: 0.5; color: white;'
      >
        <a-text
          value='Press V to show projections or controls'
          position='0 10 0'
          width='150'
          height='auto'
          align='center'
          color={this.#themeProvider.getPrimaryFontColor()}
        />
        <a-entity
          geometry='primitive: plane; width:170; height:30;'
          position='0 0 -1'
          material='opacity: 0.9; color: white;'
        />
      </a-entity>
    ) */
    // add file title
    elements.titles[1] = {
      key: 195001 + this.#id,
      text: {
        value: this.#histogram.fName,
        color: this.#themeProvider.getPrimaryFontColor()
      },
      rotation: {
        x: 0,
        y: 330,
        z: 0
      },
      position: {
        x: this.#xOffset + 120,
        y: 16.5,
        z: this.#yOffset - 70
      },
      innerText: {
        value: 'Press buttonY to show projections or controls',
        color: this.#themeProvider.getPrimaryFontColor()
      }
    } /* (
      <a-entity
        key={21000 + count + this.#id}
        // geometry='primitive: plane; width:170; height:30;'
        text={`value: ${
          this.#histogram.fName
        }; color: ${this.#themeProvider.getPrimaryFontColor()}; width: 230; height:auto; align:center;`}
        rotation={`${0} ${330} ${0}`}
        position={`${this.#xOffset + 120} ${16.5} ${this.#yOffset - 70}`}
        // material='opacity: 0.5; color: white;'
      >
        <a-text
          value='Press buttonY to show projections or controls'
          position='0 10 0'
          width='150'
          height='auto'
          align='center'
          color={this.#themeProvider.getPrimaryFontColor()}
        />
        <a-entity
          geometry='primitive: plane; width:170; height:30;'
          position='0 0 -1'
          material='opacity: 0.9; color: white;'
        />
      </a-entity>
    ) */
    // add banner
    count++
    const bannerWidthX =
      this.#range > this.#xLimit ? this.#xLimit + 1 : this.#range
    const bannerWidthY =
      this.#range > this.#yLimit ? this.#yLimit + 1 : this.#range
    elements.ground = {
      key: 22500 + count + this.#id,
      scale: {
        width: bannerWidthX + 8,
        height: 0.2,
        depth: bannerWidthY + 8
      },
      animation: {
        fromX: this.#xOffset + bannerWidthX * 0.5 - 1 + this.#shift.xOffset,
        fromY: -0.5,
        fromZ: this.#yOffset + bannerWidthY * 0.5 - 0.5 + this.#shift.yOffset,
        toX: this.#xOffset + bannerWidthX * 0.5 - 1,
        toY: -0.5,
        toZ: this.#yOffset + bannerWidthY * 0.5 - 0.5
      }
    } /* (
      <a-box
        key={22500 + count + this.#id}
        width={bannerWidthX + 8}
        material='color: #171717'
        height={0.2}
        depth={bannerWidthY + 8}
        // position={`${this.#xOffset + 2.5} -0.5 ${this.#yOffset + 2.5}`}
        animation={`property: position; from: ${
          this.#xOffset + bannerWidthX * 0.5 - 1 + this.#shift.xOffset
        }
        -0.5
        ${this.#yOffset + bannerWidthY * 0.5 - 0.5 + this.#shift.yOffset};
         to:
         ${this.#xOffset + bannerWidthX * 0.5 - 1} -0.5 ${
          this.#yOffset + bannerWidthY * 0.5 - 0.5
        }; dur: 1000;`}
      />
    ) */
    /*
    elements.banners[0] = (
      <a-entity
        key={4500 + count + 2}
        id='th2-banner'
        // text={`value: ""; color: white; width: 230; height:auto; align:center;`}
        // rotation={`${0} ${330} ${0}`}
        position={`${this.#xOffset + this.#range / 2 + 2.5} 4.5 ${
          maxYLength + 8
        }`}
      >
        <a-entity rotation='0 180 0' position='0 4 0' text={`value: "";`} />
        <a-entity rotation='0 180 0' position='0 2 0' text={`value: "";`} />
        <a-entity rotation='0 180 0' position='0 0 0' text={`value: "";`} />
        <a-entity
          geometry='primitive: box; width:15; height:15; depth:0.1;'
          position='0 0 0.5'
          material='color: black;'
        />
      </a-entity>
    )
     */
    // add banner with mappings
    count++
    elements.banners[1] = {
      key: 240000 + count + this.#id,
      id: 'th-mapping',
      geometry: {
        width: 0.1,
        height: 15,
        depth: 22
      },
      position: {
        x: maxXLength + 8,
        y: 7,
        z: this.#yOffset + this.#range / 2 + 2.5
      },
      material: {
        color: '#ffffff',
        transparent: true
      }
    } /* (
      <a-box
        key={24000 + count + this.#id}
        id='th-mapping'
        geometry='width:0.1; height:15; depth:22;'
        // text={`value: ${histogram.fName}; color: black; width: 230; height:auto; align:center;`}
        // rotation={`${0} ${330} ${0}`}
        position={`${maxXLength + 8} 7
       ${this.#yOffset + this.#range / 2 + 2.5}`}
        material='transparent:true'
      />
    ) */
    count++
    elements.banners[2] = {
      key: 255000 + count + this.#id,
      id: 'th-background',
      geometry: {
        width: 0.2,
        height: 16,
        depth: 24
      },
      position: {
        x: maxXLength + 8.2,
        y: 7,
        z: this.#yOffset + 1 + this.#range / 2 + 1.5
      },
      material: {
        color: '#ffffff',
        transparent: false
      }
    } /* (
      <a-box
        key={25500 + count + this.#id}
        id='th-background'
        geometry='width:0.2; height:16; depth:24;'
        // text={`value: ${histogram.fName}; color: black; width: 230; height:auto; align:center;`}
        // rotation={`${0} ${330} ${0}`}
        position={`${maxXLength + 8.2} 7
       ${this.#yOffset + 1 + this.#range / 2 + 1.5}`}
        material='color: white, transparent:false'
      />
    ) */

    // add normal and reversed labels into elements
    elements.labels = [...normalAxis, ...reversedAxis]

    return elements
  }

  /**
   * Funkcia mapuje hodnotu obsahu do určitého rozsahu.
   * @param {number} binContent - Aktuálny obsah binu, ktorý obsahuje objekt histogramu
   * @param {number} maxContent - Maximálny možný obsah, ktorý bin môže nadobudnúť
   * @return {number} binContent - Vracia optimalizovaný obsah v potrebnom rozsahu
   */
  getElements(projectionFunction) {
    // generate all necessary element
    let elements = {}
    if (this.#type.includes('TH2')) {
      elements = this.#createTH2Histogram(projectionFunction)
    } else if (this.#type.includes('TH3')) {
      elements = this.#createTH3Histogram()
    }
    return elements
  }

  /**
   * Funkcia mapuje hodnotu obsahu do určitého rozsahu.
   * @param {number} binContent - Aktuálny obsah binu, ktorý obsahuje objekt histogramu
   * @param {number} maxContent - Maximálny možný obsah, ktorý bin môže nadobudnúť
   * @return {number} binContent - Vracia optimalizovaný obsah v potrebnom rozsahu
   */
  #optimizeBinContent = (binContent, maxContent) => {
    if (binContent <= 0) return 0
    if (maxContent > 1) {
      // round to 2 decimal numbers
      binContent =
        Math.round((binContent / maxContent + Number.EPSILON) * 100) / 100
    }

    return binContent
  }

  updateSection(axisOffset, increment, defaultRange) {
    if (this.#type.includes('TH3'))
      return this.#editTH3OffsetByOffset(axisOffset, increment, defaultRange)
    else if (this.#type.includes('TH2'))
      return this.#editTH2OffsetByOffset(axisOffset, increment, defaultRange)
  }

  /**
   * Funkcia upraví ofset, určí všetky podmienky a na základe nich vráti modifikované ofsety.
   * @param {Object} limits - Objekt, obsahujúci najvyššie povolené ofsety pre histogram
   * @param {Object} offsets - Objekt, obsahujúci všetky aktuálne ofsety pre histogram
   * @param {number} increment - Hodnota obsahujúca hodnotu, o ktoru sa modifikuje ofset
   * @param {string} axisOffset - Názov ofsetu, ktorý je potrebné modifikovať
   * @param {boolean} defaultRange - Ak je True, inkrement je predvolený (1), ak False použije sa hodnota parametra increment
   * @return {Object} result - Vracia objekt obsahujúci všetky aktualizované ofsety
   */
  #getValidOffset = (limits, offsets, increment, axisOffset, defaultRange) => {
    this.#shift = {
      xOffset: 0,
      yOffset: 0,
      zOffset: 0
    }
    if (increment) {
      if (defaultRange) {
        offsets[axisOffset] += 1
        this.#shift[axisOffset] = -1
      } else {
        offsets[axisOffset] += offsets.range
        this.#shift[axisOffset] = -offsets.range
      }
      if (limits[axisOffset] <= offsets.range) {
        offsets[axisOffset] = 1
        this.#shift[axisOffset] = 0
        return offsets
      }
      if (offsets[axisOffset] > limits[axisOffset] - offsets.range) {
        offsets[axisOffset] = limits[axisOffset] - offsets.range
        this.#shift[axisOffset] = 0
        return offsets
      }
    } else {
      if (defaultRange) {
        offsets[axisOffset] -= 1
        this.#shift[axisOffset] = 1
      } else {
        offsets[axisOffset] -= offsets.range
        this.#shift[axisOffset] = offsets.range
      }
      if (offsets[axisOffset] < 1) {
        offsets[axisOffset] = 1
        this.#shift[axisOffset] = 0
        return offsets
      }
    }
    return offsets
  }

  /**
   * Funkcia upravuje ofset sekcie pre TH3 histogram.
   * @param {string} axisOffset - Označenie ofsetu, ktorý je potrebné modifikovať
   * @param {number} increment - Hodnota, o ktorú je potrebné upraviť ofset
   * @param {boolean} defaultRange - Ak je True, inkrement je predvolený (1), ak False použije sa hodnota parametra increment
   * @return {Object} result - Vracia objekt obsahujúci všetky aktualizované ofsety
   */
  #editTH3OffsetByOffset = (axisOffset, increment, defaultRange) => {
    // set object with limits
    const limits = {
      xOffset: this.#xLimit,
      yOffset: this.#yLimit,
      zOffset: this.#zLimit
    }
    // set object with current offsets
    const offsets = {
      name: 'TH3',
      xOffset: this.#xOffset,
      yOffset: this.#yOffset,
      zOffset: this.#zOffset,
      range: this.#range
    }
    const result = this.#getValidOffset(
      limits,
      offsets,
      increment,
      axisOffset,
      defaultRange
    )
    this.#xOffset = result.xOffset
    this.#yOffset = result.yOffset
    this.#zOffset = result.zOffset
    // return result as object
    return result
  }

  /**
   * Funkcia určuje upravený vzhľad binov, závislom na špeciálnej funkcií.
   * @param {string} projectionFunction - Definuje názov módu, ktorý bude použitý
   * @param {number} xMin - X súradnica binu, ktorý je potrebné upraviť
   * @param {number} yMin - Y súradnica binu, ktorý je potrebné upraviť
   * @return {string} color - Vracia HEX hodnotu farby, ktorá binu prislúcha
   */
  #projectionFunction = (projectionFunction, xMin, yMin) => {
    if (projectionFunction === 'feet' && this.#projections !== null) {
      const xIndex = xMin / this.#xWidth
      const yIndex = yMin / this.#yWidth
      try {
        const projectionHistogram =
          this.#projections.fFolders.arr[xIndex].fFolders.arr[yIndex].fFolders
            .arr[0]
        const LineColor = projectionHistogram.fFunctions.arr[0].fLineColor

        if (LineColor === 600) {
          return 'blue'
        } else if (LineColor === 632) {
          return 'red'
        }
      } catch (e) {
        return ''
      }
    }
    return ''
  }

  /**
   * Funkcia upravuje ofset sekcie pre TH2 histogram.
   * @param {string} axisOffset - Označenie ofsetu, ktorý je potrebné modifikovať
   * @param {number} increment - Hodnota, o ktorú je potrebné upraviť ofset
   * @param {boolean} defaultRange - Ak je True, inkrement je predvolený (1), ak False použije sa hodnota parametra increment
   * @return {Object} result - Vracia objekt obsahujúci všetky aktualizované ofsety
   */
  #editTH2OffsetByOffset = (axisOffset, increment, defaultRange) => {
    // set object with limits
    const limits = {
      xOffset: this.#xLimit,
      yOffset: this.#yLimit
    }
    // set object with current offsets
    const offsets = {
      name: 'TH2',
      xOffset: this.#xOffset,
      yOffset: this.#yOffset,
      range: this.#range
    }
    const result = this.#getValidOffset(
      limits,
      offsets,
      increment,
      axisOffset,
      defaultRange
    )
    this.#xOffset = result.xOffset
    this.#yOffset = result.yOffset
    // return result as object
    return result
  }

  /**
   * Funkcia určuje predošlú veľkosť binu, Pri zmene veľkosti binu sa bin upráví z predošlej hodnoty na novú hodnotu.
   * @param {string} id - Identifikátor binu
   * @return {number} result - Vracia predošlú veľkosť binu
   */
  #getPreviousBinScales = (id) => {
    const previousBin = this.#previousBins.filter(
      (prevBin) => prevBin.key === id
    )
    if (previousBin[0] && previousBin[0].scale) {
      return previousBin[0].scale
    }
    return 0
  }

  /**
   * Funkcia určuje predošlú farbu binu, Pri zmene farby binu sa bin zmení z predošlej farby na novú farbu.
   * @param {string} id - Identifikátor binu
   * @return {string} result - Vracia HEX hodnotu predošlej farby binu
   */
  #getPreviousBinColor = (id) => {
    const previousBin = this.#previousBins.filter(
      (prevBin) => prevBin.key === id
    )
    if (previousBin[0] && previousBin[0].scale) {
      return previousBin[0].color
    }
    return '#ffffff'
  }
}
